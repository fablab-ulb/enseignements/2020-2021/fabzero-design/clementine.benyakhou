# 4. MODULE 4 : DECOUPE LASER

## DECOUPE ASSISTEE PAR ORDINATEUR

Qu'est ce que c'est ? Comment ça marche ? 

Pour faire une bonne découpe laser il y a des principes de base à savoir avant tout : causons **vectorisation** ! La différence entre un [dessin vectoriel](https://fr.wikipedia.org/wiki/Image_vectorielle) et un [dessin matriciel](https://fr.wikipedia.org/wiki/Image_matricielle), c'est que le dessin matriciel est constitué d'une matrice de pixel, en gros une sorte de ''carte de points colorés" juxtaposés les uns à côté des autres. Au contraire, le dessin vectoriel est fait de ligne géométrique. Pour la découpe laser, c'est ce dernier qui nous interesse ! Les machines ne savent pas identifier les contours des dessins à partir d'un fichier matriciel ou Bitmap. Il faudra donc travailler avec des vecteurs. Un autre avantages de travailler avec ce type de dessin c'est concrètement qu'on peut agrandir notre dessin à l'infini sans perte de qualité (ce que le dessin matriciel ne fait pas, à la fin l'agrandissement sera un applat de couleur beaucoup trop pixelisé pour être lisible). En vectoriel, on garantie donc une qualité constante, et dans tous les cas, les machines laser n'accèptent que ce format. 

Lorsqu'on parle de **découpe assistée par ordinateur**, on désigne par là l'idée que nous concevons un modèle de dessin sur l'ordinateur qu'ensuite nous transmettons à la machine pour que celle-ci le reproduise avec son laser sur les **matériaux de notre choix**, en découpant ou gravant. 

## PARLONS MATERIAUX

Lorsque je dis "matériaux de notre choix", je veux bien sur parler des matériaux recommandés d'utiliser à cet effet, tous ne sont pas bons à cela et il est essentiel de bien s'informer avant pour ne pas casser la machine ou avoir des conséquences néfastes sur la santé. Je vais vous énoncer la liste de ce qu'il faut savoir pour cela : 

### matériaux recommandés
- bois contreplaqué de 3 ou 4 mm idéalement (attention les bois fume beaucoup lors des découpes)
- acrylique (souvent appelée plexiglass, très simple à découper) 
- papier
- carton 
- certains textiles (voir explication plus bas)

### matériaux déconseillés
- MDF (produit une résine très toxique lors de la découpe qui colle la machine en plus de cela et peut la casser)
- ABS, PS (polystyrène qui produit une fumée très nocive et fond facilement)
- polyétylène épais PE, PET, PP (les découpes ne se font pas proprement)
- les matériaux composites à base de fibre (fibre de verre, fibre de carbone, ... Car la poussière est très nocive)
- les métaux (découpe très difficile voir souvent impossible)

### matériaux interdits
- le PVC (produit une fumée toxique qui détériore la machine, il faut faire très attention parce l'apparence est similaire à celle du plexi !!)
- le cuivre (car il reflète totalement le faisseau laser sur la machine elle-même, ce qui pourrait la détruire)
- Téflon (PTFE : fumée acide, et très nocive)
- le vinyle ou le simili-cuir (produit des fumées acides, si présence de chlorine, qui détruisent la machine)
- Résine phénolique, époxy (fumée très nocive aussi)


## GRAVURE OU DECOUPE

Le FabLab dispose de **deux machines** toutes deux disposer à faire de la gravure ou de la découpe. Quelle est la différence ? La gravure marque et creuse le matériau mais pas sur l'entièreté de son épaisseur. C'est à nous de decider de cette profondeur en faisant nos propres réglages. La découpe scinde en deux le matériau. Si on est pas certain du resultat qu'on optiendra, il est préférable de faire un teste avant sur le dit matériau, pour cela il existe déjà des patterns tout faits et téléchargeables pour tester les différents réglages de **puissance et vitesse du laser** (ce qui influencera l'épaisseur de la découpe/gravure). Ce ne sont pas ceux-ci qu'utilisent le FabLab, mais le principe est le même : 

![pattern](../images/pattern.jpg)

Lorsqu'on crée notre image vectorisée, on doit mettre une **couleur** par type de traits dont les réglages seront différents. On peut, par exemple, sur un même dessin faire de la découpe et de la gravure, mais il sera alors essentiel pour la machine de savoir distinguer les deux et les paramètrer en fonction du résultat escompté. Ainsi on applique une couleur à chacun comme on peut le voir sur l'image ci-dessus, chaque couleur sera paramètrée en fonction de sa puissance **(%)** et sa vitesse **(F)** et permettra à la machine de savoir quoi faire.

## LES MACHINES DU FABLAB

Comme je le disais, le FabLab dispose de deux machines à découpe laser :

![machines](../images/machines.jpg)

- la première est la **lasersaur**. Il s'agit d'une machine en open-source qu'ils ont construit eux-même. Cette machine, aux vues de sa taille, permet de travailler sur une grande surface et faire des découpes de qualité de par sa puissance. Elle possède un espace de découpe de 122 x 61 cm, avec une hauteur maximal de 12 cm (bien que ce ne soit pas recommandé), et une vitesse maximale de 6000 mm/min (ce qui là aussi n'est pas conseillé car la machine n'aurait pas le temps de prendre assez de vitesse avant la fin de la pièce, cela donnerait des irrégularités). La puissance de son laser est de 100 Watt infrarouge. L'interface utilisateur nécessaire au fonctionnement de la machine est la **Drive BoardApp**. Cette interface gère les fichier de tipe SVG ou DXF. Les fichiers SVG ont tendance à mieux fonctionner généralement. On peut travailler avec le wifi ou connecter l'ordinateur avec un câble ethernet RG45. Si ça ne fonctionne pas, il est nécessaire de passer sur le logiciel **INKSCAPE**.

- La seconde est la **Muse Full Spectrum**. Il s'agit d'une machine conçue en usine, dont la marque est l'une des meilleures dans ce domaine. Elle est bien plus petite donc utilisée plus pour des petites pièces et principalement de la gravure (car moins puissante). Elle a une surface de travail de 50x30cm, un laser bien moins puissant de 40 watt, son laser est un tube à CO2 (infrarouge avec un pointeur, ce qui rend l'utilisation bien plus simple. Elle possède un écran tactile qui la rend simple d'utilisation et fonctionne avec une interface **Retina ENGRAVE** par wifi ou connection PC, par ailleurs elle ne prend en charge que les fichiers SVG mais parvient à lire des images matricielles BMP, JPG, PNG, TIFF et PDF.


## PRECAUTIONS D'UTILISATION 

Avant tout, il faut s'assurer de connaitre parfaitement le matériau que l'on souhaite découper/graver pour ne pas endommager la machine ou sa santé. Puis il faut s'assurer de toujours ouvrir la vanne d’air comprimé avant de faire fonctionner la machine (auquel cas celle-ci allumera un voyant orange si ce n'est pas fait), allumer l'extracteur de fumée aupréalable également et savoir ou se trouve le bouton d'urgence (gros bouton rouge sur lequel on appuie en cas de soucis pour la **Lasersaur** et interface sur l'écran Stop/pause pour la **Muse FullSpectrum**). Enfin, avant de démarrer la machine il faut s'assurer de savoir où trouver un extincteur au CO2 en cas de gros problème et le conserver à proximité. Si tout cela est convenablement appliqué, on peu démarrer la découpe laser. Ci dessous, les boutons à connaitre : 

![photo boutons](../images/boutons.jpg)

1: arrivée d'air comprimé Lasersaur
2: bouton arrêt d'urgence Lasersaur
3: pompe à air comprimé Muse FullSpectrum
4: interface pause/stop Muse FullSpectrum

## SE CONNECTER AUX MACHINES

On ne peut connecter qu'un seul PC par machine, pour cela, on peut utiliser le câble RG45 pour connecter l'ordinateur à la machine où utiliser le résau wifi. Il faut se connecter au **réseau** nommé "LaserCutter" avec le **mot de passe** "fablabULB2019". Si on utilise la Lasersaur, on utilisera l'adresse : http://192.168.1.3:4444, si on utilise la Muse FullSpectrum, on devra se rendre sur l'adresse : http://192.168.1.4.

## FAIRE FONCTIONNER LES MACHINES 

- 1 : démarrage : Pour préparer la machine et faire en sorte que tout fonctionne bien, il faut deja l'**allumer** (dans le cas de la Lasersaur) en tournant le bouton d'arrêt d'urgence dans le sens des flèches blanches. Une fois la machine connectée, on se connecte en Wifi ou par câble à celle-ci, puis on demande au laser de "retourner à l'origine". Pour cela, il faut cliquer sur le bouton **Run Homing cycle** (petit logo de maison). Puis vérifier le status de la machine (le logo doit être vert, si il est orange, la vanne n'est sans doute pas ouverte, mais si il est rouge il y a un problème dans la machine). 

- 2 : mettre en position : D'abord, on doit installer le matériau que l'on souhaite découper dans la machine. Puis une fois le matériau bien callé, on déplace la tête pour couper au bon endroit. Par sécurité, il faut deja relever la lentille pour éviter le risque de collision qui pourrait endommager la machine, puis on positionne la tête avec le bouton **move** ou **jog** à l'endroit qu'on souhaite sur le matériau. Enfin, on règle la focale avec un petit support mis à disposition (idéalement on la place à 15mm de distance, mais si le matériau est plus épais, il est préférable de viser la surface dans la demi épaisseur du matériau). 

- 3 : faire les réglages : Si on le souhaite, on peut déplacer le dessin où on veut sur la feuille, pour cela, il faut appuyer sur le bouton **Offset**. Ensuite, on vérifie que le dessin ne dépasse pas du matériau, pour cela c'est simple, on appuie sur le bouton **Run bounding box**. Si tout est bien positionné, on peut passer au calibrage de la découpe : on séléctionne les couleurs de de traits en appuyant sur le bouton **+**. On calibre autant de couleur qu'il y a de type de découpe/gravure. A partir de là, on règle la vitesse **%** et la puissance **F**. Plus c'est rapide et puisant, plus la découpe est "profonde". Si on est pas certain, comme je le disais auparavant, on peut imprimer le pattern sur le matériaux pour tester, ou consulter des [exemples de réglages](https://github.com/nortd/lasersaur/wiki/materials). Par défaut, la puissance est règlée sur F1500. 

- 4 : démarrage : on peut lancer l'impression !! Si il y a de la gravure puis de la découpe, il vaut mieux commencer par la gravure, sinon la pièce découpée risque de bouger. Et c'est parti ! Bon après ça c'est la théorie, maintenant la pratique : 

# TRAVAIL D'EXPERIMENTATION : 

Afin de vraiment connaitre les machines, il est essentiel d'appliquer la théorie. Pour cela, il nous à été proposé un exercice : réaliser un luminaire sur une feuille de **polypropylène**. Idéalement on découpe le polypropylène à une vitesse de 80% à 2600 m/s. Mais ça c'est dans le cas d'une forme simple, il vaut mieux baisser la vitesse et aussi la puissance pour avoir un meilleur résultat. Chaque matériau réagit à sa manière. Le mieux reste toujours de faire des tests au préalable. 

Pour réaliser ce luminaire, il faudra revisiter l'objet choisi (en l'occurance pour ma parte la lampe Gherpe de Superstudio). Ce n'est pas une chose simpel dans mon cas, car il est compliqué de revisiter une lampe pour en faire une lampe. Je pense développer essentiellement la notion de rotation et modularité, qui est celle qui m'interesse le plus dans mon objet. En plus de cela, le polypropylène est une matière flexible et translucide, idéale donc pour jouer sur la lumière. 

![références](../images/refs.jpg)

Idéalement, j'aimerai m'inspirer de ces références : Avec l'idée de conserver la modularité et les arcs de la lampes, mais en prolongeant ces arcs pour en faire des cercles complets. La souplesse du matériaux se prète aux cercles, mais pas du tout à l'axe qui les liera. C'est certainement ce qui sera mon plus gros problème. POur cela, je vais réfléchir à la manière de créer un axe de rotation sur une base 2D souple (un sacré enjeux) !


## IMAGINER UN AXE DE ROTATION

Pour permettre la modularité de mon luminaire, je cherche à recréer un axe de rotation. Pour cela, je suis passée par plusieurs étapes de recherche. Je vais vous détailler celles-ci : 

- 1 : Axe de rotation circulaire : La première idée qui me vient en tête, c'est de créer un axe circulaire, le matériau en polypropylène est souple et permet cela, la seule contrainte sera de trouver comment souder les deux bords du rectangle pour en faire un cylindre. Pour cela, j'ai cherchée différents types de système d'encoche et j'ai fais des tests : 

![assemblage encoche](../images/encoche.jpg)

Conclusion : l'axe de rotation est trop fin, difficile à assembler mais si on réduit le nombre d'encoche, il ne conserve pas correctement sa forme cylindrique, ce n'est donc pas idéale. Un autre gros problème, c'est les encoches et les rebords qui dépassent. La forme n'est pas parfaitement cylidrique mais plus en forme de goute. Ca ne marche pas, je tente une autre forme. 


- 2 : Axe de rotation cruciforme : je tente de faire un axe cruciforme qui s'emboite dans des cercles pour venir le fixer. En théorie ca marche assez bien mais sur des objets avec une matérialité plus rigide. Avec le polypropylène, ce sera certainement plus compliqué mais j'essaie de le faire quand même. D'ailleurs je me suis inspirée du mécanisme de la machine à coudre faite en découpe bois qui se trouve au FabLab. J'ai donc fais les tests suivants : 

![assemblage cruciforme](../images/cercle.jpg)

Conclusion : je n'utilise pas le polypropylène fourni par le FabLab pour mes tests, mais du rodoïde transparents qui est le matériau le plus semblable que j'ai sous la main. Le rodoïde est plus souple que le polypropylène fournit. sa marche mais ca demanderait plusieurs objet perpendiculaire pour fixer la forme, ce n'est pas idéal, j'en souhaiterai 2 uniquement (1 à chaque bout). Je vais voir si je trouve d'autres méthodes.

## UPDATE : GRAND AXE

Je viens de trouver une autre méthode ! Plutôt que d'avoir un axe autour duquel les arcs s'articulent à 360° en tournant, j'ai imaginé un autre type d'axe : un grand axe Je m'explique : l'axe que je m'entetais à faire posait problème de par sa flexibilité et sa taille trop restreinte J'ai choisi de prendre le problème à l'envers et pour cela, j'ai d'abbord agrandi l'axe, puis imaginé des arcs/cercles qui ne tournent non plus autour mais **DANS** l'axe de rotation. Vous comprendrez mieu en regardant les images de mon test ci-dessous. 

![grand axe](../images/luminaire.jpg)

Enfait, il s'agit d'une sorte de principe en "collier de perle". J'ai dessinée des cercles que j'ai plié en deux, puis un long rectangle qui formera le cylindre/axe de rotation, une fois replié (à l'aide des encoches et des languettes). J'ai ensuite fait une fente dans chaqu'un des demis-cercles permettant d'enfiller le rectangle dans ceux-ci (les cercles doivent être plus gros que la bande qui formera l'axe). Une fois les cercles (demis-cercles car pliés) tous passé, on referme le cylindre avec les languettes (faisable car moins haut et plus large que lors du test de l'assemblage en encoche). On obtient alors une lampe modulable dont les cercles peuvent glisser le long de l'axe, s'ouvrir plus ou moins grand et donc gérer l'intensité lumineuse. Ainsi on obtient une lampe réinterprétée de Gherpe (par Superstudio), on y retrouve l'idée d'axe de rotation, l'agencement de différentes formes circulaires et la gestion de la luminosité. Là aussi il s'agit d'une lampe interactive. Maintenant, place à l'application de mon idée ! 


# CREATION 2D POUR LA DECOUPE

## ILLUSTRATOR 

Pour générer une planimètrie de la lampe (2D) afin de la découper au laser sous sa forme déployé, je peux passer par plusieurs procédé, je vais tenter de la faire directement dans **illustrator** comme la forme est simple. Vous pouvez observer le résultat ci-dessous. Idéalement on peut passer par un logiciel de CAO. Je vais voir si cela fonctionne, avec illustrator, on obtient directement un document au format SVG, c'est le format qui est le plus compatible avec l'impression 3D comme je l'expliquais plus haut, car il s'agit d'un logiciel qui utilise la vectorisation. 

![illustrator](../images/illustrator.JPG)

Pour ce faire rien de plus simple, je n'ai utilisé que trois outils : ceux qui sont encerclés en jaune à gauche. L'outil cercle/carré pour les formes de base, l'outil ligne et les ciseaux pour supprimer les modifications (par exemple, les restes du rectangle à l'endroit des languettes pour fermer le cylindre). J'utilise deux couleurs : le rouge pour les gravures et le noir pour les découpes. Ca me permettra plus tard dans **Inkscape** de pouvoir faire varier les paramètres de découpe de chaque élément appartenant à une même couleur. J'avais prévu de faire 14 cercles mais j'en découperai 2 de plus histoire de ne pas en manquer au cas où je me sois trompée dans mes calculs. Une fois mes formes faites, j'enregistre au format SVG, la première étape est faite. 

![vérification](../images/inskape1.JPG)

Pour faire la suite des étapes, je télécharge **inkscape** et j'ouvre mon fichier SVG. Parfait ! celui-ci s'ouvre directement, et quand je clique sur les éléments, ils sont bien indépendants les uns des autres puisque je peux les séléctionner (comme on le voit ci-dessus), ça veut dire que c'est bon signe, car c'est assez fréquent lors de la conversion en SVG (si on travaille avec un autre format) que le fichier se groupe et qu'il ne soit plus possible de distinguer les couleurs ou séparer les objets indépendants. 

## DRIVE BOARDAPP

Le passage par Inkscape n'était pas nécessaire mais m'a permi de voir que mon illustrator était juste et me rassurer pour la suite. Maintenant j'attaque l'étape finale : la découpe et la gravure au laser ! Pour cela, je dépose mon document SVG sur une clef USB que je viens connecter à l'ordinateur qui est relié à la Lasersaur du FabLab. Je l'ouvre sur l'application **Drive BoardApp** pour qu'elle donne les commandes à la machine laser. Mon fichier s'ouvre parfaitement, avant toute chose, j'allume, je règle la vitesse et la puissance de mes découpes pour ne pas oublier de la faire après. Pour cela j'appuie sur "+" pour séléctionner la couleur correspondante, puis je choisi de conserver la vitesse F à 1500 (paramètre par défaut) et je détermine ma puissance : 20 pour les gravures (plis de mes demis-cercles) et 46 pour les découpes). J'avoue ne pas avoir eu à faire les tests pour la puissance de découpe parce que certaines personnes les avaient déjà fait, autrement j'aurais du faire quelques tests aupréalable. La photo ci-dessous n'est d'ailleurs pas le paramètrage final, je me suis rappelée entre-temps qu'il faut mettre les gravures en premier pour que limiter le risque de déplacement de la feuille, les gravures "rouges" passaient donc avant les découpes "noires". 

![réglages finaux](../images/découpe.jpg)

Ensuite, je fais les différentes étapes expliquées au début de ce module : je clique sur la maison pour remettre le laser à son point d'origine et je règle le positionnement de mon dessin sur la feuille à l'aide des boutons entourrés en jaune. Je vérifie que mon dessin rentre sur la feuille en faisant faire le tour du cadre de mon dessin au laser. C'est le cas ! Je vais pouvoir commencer ma découpe mais avant il faut que j'allume toutes les machines. Lorsque cela est fait, le bouton "statut" (entourré en rouge) passe au vert, si ce n'est pas le cas, il faut cliquer dessus pour voir d'où vient l'erreur. 

Les machines sont allumées, mon dessin est bien paramétré, je peux lancer la découpe. Ci-dessous une vidéo de ce qu'il se passe en temps réel lors de la découpe sur l'application **Drive BoardApp**. On voit le curseur qui correspond à l'emplacement du laser se déplacer en même temps que ce dernier. 

![vidéo découpe](../images/video-1604049582_pqSwd82E.mp4)

## ASSEMBLAGE DE LA DECOUPE

Une fois les pièces découpées, il faut laisser la machine aspirer toute la fumée avant d'ouvrir pour récupérer les découpes. Cela prend une quinzaine de minutes si on ne veux pas respirer les fumées toxiques liées à la découpe du polypropylène. Ensuite on peut voir le résultat : 

![assemblage](../images/assemb.jpg)

- 1 : les pièces sont récupérées et détachées de la feuille de polypropylène. Pour ma part pas besoin de les détacher, la découpe était assée forte pour les détacher directement de la feuille. Je suis satisfaite, les gravures sont parfaites pour pouvoir les plier aisément sans faire rompre le matériau.

- 2 : pliage des cercles en demis-cercles : il suffit de les plier en faisant attention au sens de la gravure pour s'assurer qu'ils adoptent une force en "V". Au final, je n'en ai besoin que de 15. Il m'en reste un au cas où.

- 3 : enfilage des demis cercles : les demis cercles sont enfilés sur le rectangle qui servira d'axe par la suite, je le glisse dans la fente de chacun d'eux. Malgré les millimètres prévu en plus, il faut un peu forcer pour les passer dedans mais une fois à l'intérieur, ça va.

![assemblage](../images/assemb2.jpg)

- 4 : assemblage vue de haut : le luminaire aborde une forme d'étoile avec les demis-cercles qui se relient de bout en bout.

- 5 : La lampe vue de profil adopte une forme de "méduse", l'épaisseur du matériau accentue cet effet avec son aspect translucide. 

- 6 : on voit bien le cylindre où sont enfilés les demis cercles, qui est l'axe de rotation de la lampe, et permet de moduler les demis cercles pour gérer l'ambiance lumineuse.

## DEFAUTS 

![assemblage](../images/assemb3.jpg)

Sur ces deux images on peut voir de légers défauts duent à la vitesse de découpe du matériaux (certainement trop lente). En effet, le polypropylène est un matériau qui a tendance à facilement fondre, les traces noires que l'on voit sont la conséquence d'une vitesse sans doute un peu trop basse qui fait que le laser fait légèrement fondre et bruler les bords de découpe. Idéalement il aurait falu ajouter plus de vitesse, surtout que la pièce n'était pas avec des formes compliquées, quitte à ajouter une troisième couleur pour les languettes et les entailles et là diminuer la vitesse car la machine n'aurait pas eu le temps de prendre cette vitesse car les découpes sont courtes et plus techniques. Après il suffit de limer les zones noircies ou les nettoyer un peu et cela suffit à faire disparaitre les défauts.

En soit ce n'est pas grave ni génant car très discret, mais ça reste bon à savoir si on veut un résultat parfait sans passer par là. Il faudrait aussi penser à rajouter quelques millimètres sur les encoches pour enfiler les languettes car j'ai vraiment du forcer pour les rentrer et, parfois, du insiser d'un millimètre en plus avec un cutter pour ne pas endommager les languettes. Mais ça c'est une erreur de ma part j'imaginais que le matériaux était plus souple.  

## RESULTAT 

![final](../images/final.jpg)

Malgré quelques petits défauts, ça n'a pas géné la création de la lampe. Ca reste bon à savoir pour les prochaines fois et ça demeurre une première expérience plutôt réussie. Je suis satisfaite du résultat lors de la mise en lumière. Et surtout de la rapidité de fabrication en comparaison à l'impression 3D. L'effet de la gestion lumineuse escomptée est plutôt bien réussie. 

## BONUS 

La même lampe avec une lumière verte récupéré des accessoirs d'Halloween. 

![final](../images/verte.jpg)









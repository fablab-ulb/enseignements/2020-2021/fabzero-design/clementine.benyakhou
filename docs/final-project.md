# OBJET FINAL **MUDAR**

![feuille A4 exposition](images/feuille_A4.pdf)

![final](images/FINAL_image_design.jpg)

> La lampe MUDAR est un luminaire d’ambiance interactif déployable qui permet de gérer l’ambiance en fonction des besoins de chacun. Sa forme varie selon les envies. Elle se met en place via un câble tendu du sol au plafond. L’abat-jour en papier coulisse le long du câble pour s’ouvrir ou se fermer. Cela génère des ambiances plus ou moins tamisées. Plus elle est ouverte, plus la lumière est diffuse. Plus elle est fermée, plus sa lumière est opaque. 

![modulable](images/MODULABLE.jpg)

## EXPLICATIONS 

> Ce luminaire d’ambiance en papier est une réinterprétation de la lampe «Gherpe» conçue par Superstudio et produite par Poltronova en 1966. Cette lampe fut réalisée en Perspex (plexiglas) courbé à chaud. Composée de six arcs modulables et d’un axe central de rotation, elle adopte la forme d’une ammonite. Influence naturelle et industrielle, cette lampe est l’un des premiers exemples d’objets lumineux interactifs. Elle fut produite dans un premier temps, dans un coloris transparents rose-orangé, puis, l’année suivante, en blanc translucide. J’ai choisi de réinterpréter cet objet parmi tous ceux du musée, car j’ai été séduite par sa simplicité visuelle et son mécanisme élaboré de par sa composition évolutive (qui se déploie dans l’espace et gère l’ambiance lumineuse).
> Sa création part d'un mot : "influence". L'influence est constante et réciproque. On passe notre vie à être influencé par tout, constamment, sans en avoir conscience. Entre l’homme et la nature, il y a une influence réciproque forte. L’homme, pour évoluer, a créé des outils en s’inspirant d'éléments naturels. En retour, il a impacté le développement de la nature de par ses créations. Les objets qui nous entourent aujourd’hui sont tous issus d’influence naturelle. L’exemple de la lampe «Gherpe» (mon objet de référence) en est l’illustration parfaite : elle tire sa forme des coquilles d’ammonite (mollusque céphalopode) qui se base sur le nombre d’or, un nombre décrété par l’homme mais existant à l’état naturel. Réaliser un pliage de Miura sera l’occasion de s’inspirer à mon tour des éléments naturels (plis que l’on retrouve sur les feuilles de charmes) et conserver cette idée de déploiement que j’appréciais tant dans ma référence. Cela me permettra de proposer à mon tour, un luminaire interactif et déployable, simple d'installation et innovant.


## COUT ET DUREE DE LA REALISATION 

> - [ ]  2.72 € __ papier
> 
> - [ ]  6.99 € __ ampoule
> 
> - [ ]  4.49 € __ cordon electrique
> 
> - [ ]  2.49 € __ douille
> 
> - [ ]  1.19 € __ serre-cables
> 
> - [ ]  1.59 € __ cosse-coeurs
> 
> - [ ]  3.96 € __ câble acier 4m
> 
> 
> découpe laser : 5.3 min
> 
> impression 3D :  1h30 + 4h
> 
> temps de pliage papier : 20 min
> 
> **total : 23,43 €  / ± 6 heures**


## TECHNIQUE DE REALISATION

> Si vous souhaitez réaliser à votre tour cette lampe, prennez en compte la durée et le coût de réalisation. Ensuite, il suffit de vous munir des accessoirs spécifiés ci-dessous et suivre les explications qui viennent : 


![technique2](images/technique2en1.jpg)


### L'ABAT-JOUR EN PAPIER

> - [ ]  [patron de l'abat-jour, format SVG](images/patron_pour_gravure_et_découpe__cercle_.svg)
> 
> RECOMMANDATION : Tu peux te munir de toute sorte de papier, l'essentiel étant de ne pas dépasser les limites, car le pliage deviendra plus difficile. Idéalement prends une feuille d compris entre 65 et 105 Gr/m². Tu peux aussi recycler du papier. Pour ma part, je l'ai essayé avec une notice de médicament et ça marche bien ! La seule condition c'est que le papier doit faire au minimum 500 x 600 mm et être le plus plat possible. Après il peut être aussi grand que souhaité, tant que l'homothétie du patron est conservé (les angles surtout). Libre à toi de l'agrandir ou jouer sur la forme de découpe.
> 
> A la découpeuse laser, pense à bien distinguer les éléments colorés. Fais d'abord les gravures noires (puissance 2.5%, rapidité 50% ou 2300), puis les découpes rouges (puissance 6%, rapidité 50% ou 2300). Ne découpe pas la partie bleue en pointillée, c'est juste une indication du pliage. Si ton papier est fin, réduis la puissance de gravure, quitte à faire plusieurs passes de 1.5%. Tu obtiens donc ton patron de pliage, maintenant alterne les sommets dessus-dessous en pliant le papier, en commençant par l'extrémité du cercle vers la languette de fixation. 


### LE SYSTEME LUMINEUX COULISSANT 

> - [ ]  [modélisation 3D des systèmes accroches coulissantes et presse-papiers, format STL](images/modélisation_finale_3D.stl)
> - [ ]  [gcode systèmes d'accroches coulissantes](images/tubes_coulissants__gcode_.gcode)
> - [ ]  [gcode presse-papiers](images/presse-papiers__gcode_.gcode)
> 
> RECOMMANDATION : J'ai fais deux fichiers pour les impressions 3D, car l'un ne nécessite pas de supports, l'autre si. L'idée c'est d'économiser la matière et la durée de réalisation donc tu peux lancer les systèmes d'accroches coulissantes sans support et les presses papiers à part. Tu peux largement mettre la vitesse d'impression à 130 % après la troisième couche d'impression pour gagner du temps, car les formes sont simples. 
> 
> Niveau réglages PrusaSlicer, il faut mettre les données suivantes dans tous les cas :  dans réglage d'impression > 0.2 en hauteur de couche partout, 3 partout pour la hauteur de couches solides des coques horizontales, supprimer la boucle, diminuer le remplissage à 5%. Puis dans le cas des accroches coulissantes, il faut : supprimer les supports, et dans le cas de des presse-papiers, il faut : détecter les ponts, générer des supports, 8mm en espacement du motif, 8 en couche d'interface, 8mm en espacement du motif d'interface et décocher "ne pas supporter les ponts". 

### ASSEMBLAGE 

> Il suffit de fixer le câble au sol et au plafond, ou sur le mur avec un système de tige filetée (à minimum 12 cm du mur), pour le tendre. Avant de le tendre définitivement, penser à glisser les systèmes imprimés en 3D autour (le système pour la lampe au centre). Gérer la quantité de câble nécessaire en fonction de la hauteur souhaitée, puis mettre les serre-câbles pour maintenir les cables en tension, couper le cable restant inutile. Ensuite, mettre les languettes en papier (pliée en deux) de l'abat-jour dans les presses-papiers, et glisser les cylindres de fermeture dans les systèmes d'accroches coulissantes se trouvant sur le câble. Mettre l'ampoule et tu obtiens une lampe !

.

.

.

**Tu peux aussi suivre le développement de mes recherches, pour en arriver à ce travail final, de manière chronologique pour en savoir plus.**
⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊ ⇊

_____________________________________________________________________________________________________________________________________


# RECHERCHES

Avant de débuter mon travail final, j'aimerais vous rappeler mon objet et la raison de son choix. J'ai décidé de travailler sur la lampe **Gherpe** réalisée par superstudio. Cette lampe m'a tout de suite intéressée de par sa modularité et les possibilités infinies de positionnement qu'elle invite à faire. Simple dans sa forme et son fonctionnement, elle m'a pourtant freiner plus d'une fois pour vraiment en saisir sa complexité de son mécanisme de rotation. Aujourd'hui en ayant bien travaillé dessus, je pense être en mesure de dire que j'ai bien compris cette lampe. Aussi, notre travail final consistera à réinterpréter l'objet de notre choix. Pour cela je vais d'abord expliquer ce qu'est revisiter un objet et comment le faire. 

![lampe ref](images/objfin.jpg)

## EXERCICE : REVISITER UN OBJET, COMMENT ?

### ATTITUDE VIS-A-VIS DE L'OBJET 

Le travail final commence avec une étape importante : choisir l'attitude que l'on souhaite avoir vis-à-vis de notre objet choisi pour le revisiter. Pour cela j'ai à ma disposition une liste de 5 mots, dont je dois comparer les définitions, et en extraire ce qui s'en dégage. Ces 5 mots seront définis selon 4 dictionnaires dont l'approche et l'époque varient. 

- Référence
- Influence
- Inspiration
- Hommage
- Extension

J'utiliserai entre autre des définitions venues du : **Wiktionnaire**, qui propose des définitions contemporaines, qui sont constamment modifiées et évolutives grâce aux ajouts collaboratifs. Il y aura également des définitions qui viennent du **Petit Robert**, un dictionnaire plus ancien qui renseigne principalement sur l'étymologie des mots. Puis, il y aura des définitions provenant du **Petit Larousse**, un grand dictionnaire classique français, très utilisé et diffusé dans les écoles, qui lui, recense des définitions plus "encyclopédiques". Et enfin, on trouvera des définitions issues d'un dictionnaire appelé **Le Littré**, qui date de la fin du 19 ème siècle. Ce dictionnaire apparait donc au début de l'industrialisation et offre une définition plutôt littéraire des mots.

### CHOIX DU MOT 

Après avoir lu toutes les définitions, que l'on peut trouver sur le [PDF suivant](images/design_mots_definition.pdf), je pensais d'abord avoir une préférence pour le mot "inspiration", car il s'agit certainement du mot le plus "libre" et "créatif" de tous. Les définitions sont très liées aux émotions, voir au divin. Je trouve qu'à travers ce mot, il en ressort une évocation du monde psychique très intéressant qui pourrait faire partir mon objet dans tous les sens. Ca pourrait être super chouette, mais finalement, quand j'ai tendance à avoir moins de contrainte et trop de liberté, je me sens rapidement perdue face à un nombres de possibilités de projet trop vaste ce qui ne m'aide pas du tout dans mon travail et je me noie moi-même sous mes centaines d'idées. J'ai donc relu plusieurs fois toutes les définitions, et j'ai hésité entre les mots "hommage" et "influence" mais c'est finalement le mot **"influence"** qui m'a saisie, le mot "hommage" étant trop connoté comme acte admiratif poussé.

### DEFINITIONS D'INFLUENCE, CE QUI EN RESSORT 

- Wiktionnary : action de quelqu'un ou de quelque chose qui agit sur quelqu'un ou quelque chose d'autre. Les exemples utilisés sont tous issuent d'objets littéraires connus, qui montrent essentiellement l'influence d'une personne ou d'un groupe de personnes sur d'autres. Il y a prinicpalement un rapport au pouvoir social, à l'emprise. Toutes les références littéraires utilisées sont françaises, et seules les plus récentes montrent un rapport moins antropocentré et plus scientifique : On ne parle plus de l'influence de l'Homme sur l'Homme mais on aborde des influences géographiques/géologiques/... Cela montre certainement que notre intéret pour certaine chose évolue.

- Le Petit Robert : Propose le même type de définition, mais avec des exemples non littéraires. Il apporte égallement une dimension plus ancienne et astrologique de la définition, liée à la destinée humaine et aux astres. Cette définition permet aussi de faire la comparaison entre une vieille définition du mot influence et une plus moderne, qui est l'extension de l'ancienne : ce qui est lié à la magie, aux sciences occultes. C'est aussi interessant de relevé que le Petit Robert détaille le fait que : l'action d'influencer peut être volontaire ou non. Les définitions du mot sont moins centrées sur la question du pouvoir et de la domination, mais plus sur d'autres points comme l'intellect, la morale, la personnalité, ... Il y a un aspect plus psychologique et moins accadémique dans cette définition proposée. 

- Larousse : Ce dictionnaire propose une définition très neutre du mot "influence" en comparaison aux deux précédentes. Il ne donne aucun indice quant à l'aspect péjoratif ou mélioratif de ce mot. Sans exemple, les actions exercées peuvent avoir les deux finalités, tout est questions de point de vue. J'aime davantage cette définition plus alternative. Elle n'influence pas ce mot et me semble la plus honnête, car elle caractérise l'action liée à ce mot et non la finalité qui en découle. Pour illustrer cela, on a par exemple ce même rapport au pouvoir social qui revient, mais ici ce n'est pas illustré par l'action d'une personne négative qui agit sur d'autre avec une mauvaise influence, mais simplement l'action d'une personne sur une autre qui lui fait changer son point de vue, sans renseigner sur la dimension de "bien" ou de "mal" de cette action et son impact. La définition est donc plus large et plus appropriée il me semble.

- Littré : Reprend un peu tous les points abordés dans les différents autres dictionnaires, mais en plus complet car, agrémenté d'exemple. Les exemples sont parfois assez anciens voir difficile à comprendre aujourd'hui car, en ancien français, mais je trouve que dans ce dictionnaire-ci également, l'influence n'est ni jugée comme terme péjoratif ou mélioratif, mais simplement dépendant du type d'action exercée. 

### POURQUOI CE CHOIX ?

Pour moi, une influence en art est essentielle. Les définitions proposées par les 4 dictionnaires ne renvoient que partiellement à l'idée que je me fais du mot influence. Selon moi l'influence peut être un clin d'oeil à quelque chose, sans entrer dans "l'hommage", mais évoquer quelque chose de positif qu'une chose ou une personne a eut de positif à un moment donné. Pour illustrer cet exemple rien de mieux que quelques photos. Prenons le cas de Mondrian, artiste connu de tous : 

Si on prend le meuble "Homage To Mondrian" de Shiro Kuramata, il s'agit là d'un hommage comme le montre si bien le nom de l'objet design, puisque le designer réutilise la forme propre de l'art de Mondrian. Il en va de-même pour le célèbre exemple de Yves Saint-Laurent, qui, lors de la collection automne-hiver 1965, a présenté une série de robes "hommage" à Piet Mondrian. 
![mondrian](images/mondrian.jpg)

Si on prend au contraire, Theo van Doesburg, un artiste peintre. Il apparait clairement que lui est influencé par Mondrian, il ne reprend pas ses oeuvres, ne fait pas du "Mondiran" mais en subit l'influence. D'ailleurs, on peut le voir sur Wikipédia dans la définition du mouvement **De Stijl**, où la partie le décrivant spécifie : "Dès 1912 et sa première exposition, sa peinture subit l'influence de Van Gogh puis de l'abstraction lyrique de Kandinsky. Il écrit sur le futurisme et fait des recherches sur les éléments fondamentaux. Il s'intéresse à **Piet Mondrian** dont il reprend l'esthétique à partir de 1915 puis trouve au néo-plasticisme un côté conservateur". On comprend dès lors que Mondrian a eu une influence sur le peintre qui reprend son esthétique, mais pas ses tableaux en soi. Et puis après tout, Mondrian lui-même a reçu des influences de Braque et Picasso par exemple. Aisi, "INFLUENCE" et "HOMMAGE" sont deux mots bien différents. Et dans cet exemple en particulier, l'influence d'une personne n'est pas quelque chose de négatif, au contraire. Ici, l'influence est vécue comme une ouverture à une possibilité d'aller plus loin, explorer de nouvelles limites de l'art, aborder une vision nouvelle tout en conservant ses idéologies. Pour moi "l'influence" c'est avant tout cela. Puis finalement, dans une vie, toute personne subit ou vit les influences de toute sorte de choses / personnes et ce n'est pas nécessairement une finalité en soi, mais plus une marque de l'évolution de chacun en prenant en compte l'histoire de ses interactions avec le monde qui l'on mené a en arriver là. 

### POURQUOI L'INFLUENCE PLUS QUE L'INSPIRATION ?

J'ai eu du mal à voir la différence entre influence et inspiration. Pour moi ce sont deux mots quand même très proches, mais le mot "influence" me semblait plus juste. En allant plus loin j'ai compris pourquoi. Un très bon [site](https://medium.com/@Hugojude/influence-vs-inspiration-c8f05e4755cc) explique bien cette différence. L'auteur de ce site traduit parfaitement l'idée que j'avais en tête de la distinction entre ces deux mots.

![influence/inspiration](images/influ_inspi.JPG)

Ce qui me plait dans l'idée de l'influence ce sont les termes **caractère**, **comportement** et **effet**. Dans l'inspiration on a un fort rapport à la créativité et la stimulation. Au contraire, dans l'influence, on joue davantage sur l'effet et c'est exactement sur cela que je souhaite jouer. J'aspire à modifier le caractère de ma lampe, ne pas la "dénaturer" mais plus accentuer les effets qui y sont liés.

### MON PARTI POUR CE PROJET 

Idéalement, mon ambition pour mener à terme ce projet serait de justement jouer sur ce qu'est pour moi l'influence en ne cherchant pas nécessairement à modifier la topologie de mon objet choisi (la lampe Gherpe de Superstudio), mais davantage jouer avec ses principaux atouts (ce qui m'a marqué le plus lors de son étude : la modularité, l'infinité de possibilité de positionnement, les effets lumineux et visuels, ...).

Pour cela, j'imagine partir sur l'étude d'un ou plusieurs artistes ayant joué justement avec des sujets similaires (lumière, effet visuel, modularité, etc.), et n'ayant pas spécialement un rapport à cette lampe, mais m'ayant personnellement touchés. Pourquoi des artistes ? Dans la mesure où je trouve que cette lampe possède un design soigné d'un point de vue artistique qui évoque presque une sculpture, c'est un bel objet, à la fois léger et éléguant, qu'il est difficile de modifier, car cela changerait son système de fonctionnement : par exemple, si on modifie la forme de ses arcs, la rotation ne peut plus s'effectuer et cela perd en intérêt. Par contre jouer sur sa modularité et ses variations visuelles en apportant un quelque chose en plus peut être très intéressant. Ainsi, je ferai une analyse de plusieurs oeuvres et je tenterai de voir comment je peux réinterpréter cette lampe tout en conservant son identité et en lui apportant un "plus". Pour le moment j'ai songé à plusieurs artistes qui pourraient être intéressant pour développer cette idée de projet. Parmi ceux-ci on retrouve : 

![influences idées](images/effet.jpg)

- 1. VASARELY : Qui joue mieux que lui sur les effets visuels ? C'est véritablement le père du Op Art (ou art optique). Un peu plus âgé que Superstudio, il joue sur les formes, les couleurs et les effets cinétiques. A travers son travail, on se perd dans ses tableaux. On y perçoit du mouvement, alors même que ceux-ci sont fixes. Une idée qui selon moi, s'associerait à la perfection avec mon objet modulable. De quoi apporter des effets visuels intéressants en comprenant mieux ses oeuvres. 

- 2. TURELL : Son médium de prédiléction ? La lumière et l'espace. Ca tombe bien, j'ai justement une lampe. Quoi de mieux que de ressentir les influences de Turell pour gérer encore mieux les ambiances lumineuses de cette lampe ? Dans ses travaux il nous fait oublier la notion d'espace et nous fait voyager dans des univers colorés, aux couleurs acidulées, qui nous enveloppe et nous fait perdre la notion du temps. La modularité et les filtres lumineux pourront complètement être un atout pour mon objet afin de mieux comprendre comment apporter une ambiance lumineuse spécifique. 

- 3. KUSAMA : Peintre et sculptrice, Kusama nous emmène dans des univers fantastiques à base de ronds colorés. Elle aime jouer sur la perception, troubler les espaces, avec ses formes et ses couleurs. Elle imagine des univers très psychédéliques bien reconnaissables et toujours sur des grandes échelles. A la fois artiste et plasticienne, elle façonne ses univers.

### IDEE PRINCIPALE 

Finalement, c'est en écrivant ceci que je me rends compte que les trois artistes auxquels j'avais pensé sont aux moins liés par plusieurs points : le trouble de la perception dans l'espace et les effets optiques. J'avoue être vraiment intéressée pour jouer sur ses points, je pense que ça pourrait être super intéressant associé avec le mécanisme de cette lampe. 

Le modèle Gherpe de Superstudio existe sous deux formes : la création de base (1967) en acrylique teintée de rose et un modèle plus tardif (1968) blanc. Je pensais en avoir vu d'autres sortes, mais en fait, en cherchant d'avantage, je réalise que ce ne sont pas de vrais modèles. Je pense en particulier à une version de la lampe Gherpe (ci-dessous) dont les arcs sont chacun teintés d'une couleur différente. En réalité je doute qu'il s'agisse d'une lampe de Superstudio, la forme n'a rien à voir au niveau du système de rotation et elle semble plus écrasée que le vrai modèle, je pense donc qu'il s'agit d'une ré-interprétation de l'objet par une autre personne.

![version colorée](images/926dc9122b0fde97558bf83584a42874.jpg)

Ce type de ré-interprétation par influence pourrait être super interessant en ajoutant plus d'effet visuels. Par exemple en jouant sur la superposition des couleurs, des percements, la capacité d'éclairement de la lampe, etc. pour créer des effets visuels de plus grande ampleur que cette simple ré-interprétation. 

### POUR ALLER PLUS LOIN 

J'ai trouvé un article en anglais très instructif et facile à comprendre qui parle bien de ce qu'est la référence. En voici un extrait, vous pouvez le lire en cliquant sur ce [lien](https://sites.google.com/site/digihumanlab/research/artistic-influence). Le PDF [suivant](https://www.cs.rutgers.edu/~elgammal/pub/ICCC14_Influences.pdf) va encore plus loin.

> "Finding influences and connections between artists is an important task for art. By doing so, the conversation of art continues and new intuitions about art can be made. An artist might be inspired by one painting, a body of work, or even an entire style of art. Art historians study which artists might influence others by examining the descriptive attributes of art. However, finding influences is a sophisticated process that involves studying the historical, social, and personal context related to the artist and to the art. After all, we will not know if an artist was ever truly inspired by a work unless he or she has said so. However, for the sake of finding connections and progressing through movements of art, a general consensus is agreed upon if the argument is convincing enough." **Artistic Influence - Digital Humanities Laboratory at Rutgers**

Ce que j'aime dans cet article, c'est qu'il met en avant le fait que tous les artistes ont naturellement des influences pour réaliser leurs oeuvres, mais que tant qu'ils ne le mentionnent pas, rien n'est certain, même eux ne savent surement pas toujours par qui ou quand ils ont été influencés. C'est en tout cas la vision que j'ai de l'influence. Selon moi, l'influence est quotidienne, omnisciente et pas seulement dans le milieu artistique, mais notre cerveau fait des connexions qui permettent de concevoir du "novateur" à partir du "déjà vu" en collectant des brides d'informations de choses considérées comme "intéressante". Les influences font évoluer les idées sans plagier celles-ci. Pour ce travail, c'est clairement ce que j'aimerais faire pour mon objet : le faire évoluer, sans le dénaturer. Et pour ce faire, il va falloir aller voir un peu plus en profondeur l'univers du groupe Superstudio, au-delà de leur lampe afin de conserver son style. 

### ANALYSE DU TRAVAIL DE SUPERSTUDIO 

Passons à l'analyse du travail du groupe Superstudio : un groupe que l'on a tous déjà découvert lors de nos études, associé très souvent à Archigram et Archizoom pour leur appartenance au mouvement Radical. Lorsque l'on tape le nom de ce groupe sur internet, il en ressort deux univers très distincts qui peuvent nous être utiles pour la suite du travail : 

![quadrillage](images/QUADRILLAGE.jpg)

Un univers très épuré, géométrique, où l'idée de grille/quadrillage revient souvent. Il s'agit souvent d'architecture démesurée, aussi nommée Superarchitettura. Cet univers est peu coloré, mais emprunt de jeu d'ombre, de percement et d'excroissance.

![objets design](images/COLOR.jpg)

Et un univers à plus petite échelle, très coloré, voir exubérant, à la limite du kitch, avec des formes enfantines rêveuses, un univers qui rappelerait presque celui du cirque de par ses couleurs primaires très présente. Les formes sont toutes douces, en courbes. Les objet design marquent leur présence dans l'espace. 


### REFLEXION SUR LES REFERENCES

Je me lance, j'analyse les compositions graphiques de **Vasarely**, je sens qu'il peut vraiment y voir un rendu chouette. Inutile de décrire en détail son travail et son style, je pense que tout le monde voit au moins de qui il s'agit. Si ce n'est pas le cas, il suffit de se rendre sur internet et en deux clics, on comprend rapidement l'univers qu'il a. 

Pourquoi choisir de s'influencer principalement de son travail ? Parce que son rapport a l'espace est super intéressant. Il joue énormément sur la trame et la couleur tout comme le fait le groupe Superstudio. S'influencer de sa technique de travail et la combiner à la modularité de ma lampe pourrait offrir de superbe effets visuels. 

Si on parle d'un point de vue graphique et non artistique, Vasarely utilise une technique qu'on appelle **"Halftoning"**, c'est-à-dire le "demi-ton". En reprographie cette technique est très utile, en particulier pour les imprimeries, Cela permet de donner plusieurs niveaux d'une couleur avec une simple impression monochrome. En gros plus il y a de point, plus la couleur ressort foncée, plus le point est réduit et espacé des autres, plus la couleur semble clair. Ceci est utilisé à petite échelle et donne une impression d'aplat. 

Dans le cas de son travail, c'est un peu différent de la technique du halftone, le principe est le même, mais à plus grande échelle. Elle permet de donner une sensation de volume à ses oeuvres (les couleurs plus foncées sont les ombres). Il joue sur la déformation de ses points de couleur pour en faire ressortir une impression de perspective de l'objet et ainsi faire ressortir un volume. Par exemple dans l'image ci-dessous, les déformées circulaires rouge font apparaitre une sphère en volume alors qu'il s'agit d'un simple aplat. Ces déformées sont de plus en plus proches et réduites, cela accentue l'effet de perspective. Ainsi on obtient un effet visuel super intéressant avec un simple aplat de couleur. Encore faut-il y penser.  

![halftone](images/halftone.jpg)

Après cette petite analyse, je suis tombée sur une [vidéo](https://www.youtube.com/watch?v=ejSnxMsaQUA&feature=share) d'une personne qui montre comment refaire le halftone de "base" de Vasarely rapidement et informatiquement. C'est interessant, je comprends donc qu'il va falloir jouer sur une trame. 

Pour mieux comprendre cela, j'ai fais quelques petits croquis sketchup : 

![sketchup](images/skp.jpg)

- 1 : J'ai d'abord refais un des arcs de ma lampe de manière déployée. 
- 2 : J'ai ensuite supprimé les bords inutiles du rectangle puis déterminé les axes de la grille. 
- 3 : Une fois la taille de ma grille déterminée, j'ai tracé des carrés aux tailles dégraissives. 
- 4 : J'ai ensuite supprimé les lignes de guide et mis des couleurs dans les carrés. 
- 5 : J'ai décomposé les couleurs et les formes sur différents arcs pour jouer sur les effets de lumière et de forme.

__________________________________________


## PHASE 1 : INFLUENCE VASARELY

### MODELISATION 2D INDESIGN

Pour m'assurer que je pars sur une bonne piste, je réalise déjà plusieurs tests au niveau des dessins pour permettre d'avoir une idée du résultat fini. Pour cela j'utilise **Illustrator** que je ne maitrise pas encore parfaitement pour mieux me débrouiller sur ce logiciel, et avoir un résultat directement en **SVG** efficace. J'ai donc reproduit mes différents arcs à l'échelle 1/2 de manière à m'approcher au maximum d'un résultat réaliste. J'ai ensuite intégré les dessins des découpes internes en jouant sur différents motifs propres à l'art optique. Pour cela j'ai utilisé un jeux de deux couleurs afin de distinguer les bords des arcs et les motifs intérieurs. Ainsi je peux demander à la machine de découpe laser de faire les motifs en premier, puis les contours ensuite, cela évite que le matériaux se déplace s'il est deja découpé et que mes découpes ne réussissent pas. Ci dessous, mes deux dessins 2D tests (je n'ai travaillé que sur les arcs les plus gros pour gagner du temps et me faire une idée déjà, les lignes bleues sont mes lignes de guide, elles n'apparaissent pas sur le résultat final).

### DECOUPE LASER 

![modé 2D](images/rond_carré_1.jpg) 

Pour faire une bonne découpe, j'utilise les plaques de polypropylène mises à notre disposition, en sachant que ce ne sera pas mon matériau final puisque j'aspire à travailler avec de la couleur. D'un point de vue technique, j'ai décidé de mettre F : 1500 et 45 % pour les motifs intérieurs et F : 2000, % 45 pour les contours car la machine prend de la vitesse plus facilement pour ses découpes simples. Une fois la découpe lancée, j'attends le résultat. Je me rends compte que ces valeurs n'étaient pas très bonnes puisque les contours ne sont pas très bien découpés. Je viens de comprendre que mon F était trop élevé et la machine a pris trop de vitesse, le laser n'a pas eu le temps de marquer assez profondément la plaque de polypropylène. 

![découpes](images/rond_carré_2.jpg) 

Ci-dessous les résultats : les arcs se détachent mal de la plaque, mais les motifs intérieurs sont réussis. J'ai besoin d'un petit coup de main du cutter par endroit pour tout détacher, en particulier au niveau du percement de l'axe de rotation. Mais l'effet cinétique est bien là, la réduction du motif d'arc en arc permet un effet perspectif intéressant. Par contre, l'effet "dentelle" me plait un peu moins, je trouve que les arcs sont un peu trop ajourés, le but n'est pas de voir la lampe au centre non plus. Ce n'est pas grave je vais jusqu'au bout de mon test. 

![résultats découpe](images/rond_carré_3.jpg) 

### MISE EN COULEUR 

A défaut d'utiliser des plaques de polypropylène coloré, j'ai choisi de faire mes tests sur ce même matériau translucide et ensuite ajouter une couche d'encre de Chine pour coloré le tout pour avoir une idée du résultat en couleur. Ci dessous les résultats. Pour le moment mes choix de couleur sont un peu aléatoires. 

![résultat couleur](images/rond_carré_4.jpg) 

Je n'ai pas lancé ma deuxième 2D à la découpe avec la version carrée. Je trouvais que le résultat ne correspondait pas à mes attentes et était trop ajouré. Toutefois, je suis tout de même assez satisfaite des effets cinétiques produits par la perspective comme on peut le voir ci-dessous. On voit clairement des formes en relief se dessiner sur les arcs (des ronds qui apparaissent en volume). Mais je suis assez déçue quand même de ne pas avoir le résultat souhaité. J'aime bien l'effet que les percements produisent et le jeux d'ombres et de lumière que cela induit, toutefois, je trouve le résultat légèrement trop "enfantin" et fragile. L'abondance des percements procurent à la matière une certaine fragilité, les arcs ne se tiennent plus aussi bien que lorsqu'ils sont pleins et le système de rotation ne fonctionne plus en traction puisque la matière ne force plus vers l'extérieur, donc la forme à du mal à se tenir d'elle-même en position déployée. 

![résultat 2 couleur](images/rond_carré_5.jpg) 

### CONCLUSION SUITE A CE PREMIER TEST 

Je vais certainement partir sur des matériaux plus "durs" car le jeux de percements ne me permettent pas d'avoir les propriétés physiques que je souhaite si tout est évidé. Je pense par exemple à du carton gris ou du bois (pas plus de 3 ou 4mm). Ainsi je pense pouvoir retrouver plus de rigidité dans mes arcs malgré des ouvertures. Le challenge maintenant restera de réussir à obtenir une forme de motifs répétés qui permettent pliure et solidité à la fois. Je souhaite conserver les jeux d'ombre et de lumière que provoque la lampe. Mais je la trouve trop "ouverte" pour l'instant. De plus, j'ai l'impression de rester coincée à ne "refaire" que du Vasarely sans justement m'en influencer. 

__________________________________________


## PHASE 2 : COURBER LA MATIERE

### COMMENT PLIER UN SOLIDE ?

Je me suis renseignée un peu sur comment les propriétés géométriques et mécaniques du Lattice Hinge, une technique de cintrage du bois Propriétés géométriques et mécaniques du **Lattice Hinge** qui s'inspire du rainurage traditionnel. Cette technique utilisée sur le bois ou le carton épais permet de pouvoir plier sans perdre les qualités du bois. Il existe plein de type de motifs géométriques pour cela. La taille de ceux-ci, l'épaisseur du bois et le type de motifs fera varier les différentes manières de cintrer le bois. Ainsi, il deviendra plus ou moins flexible dans la forme. Cela pourrait être super intéressant pour moi, de manière à avoir un arc solide, qui se tient, fonctionne en traction, joue sur les ouvertures et ça reste bien plus esthétique que des plaques de polypropylène. 

Un [site](https://webentwood.com/portfolio/latticehinge) parle très bien de cette pratique, c'est grâce à ce site que j'ai découvert cette manière de faire. Je me rends finalement compte que j'avais déjà vu cette technique mais, jamais en application. Il y en a d'ailleurs des chutes au FabLab. On parle aussi d'une méthode de découpe [**KERFING**](https://www.matieres-a-graver.fr/conseils-utilisation-matieres-trotec-france/matieres-de-pliage-trotec), sur ce lien on voit différentes méthodes de découpe. 

![exemple kerfting](images/ex_kerfting.jpg)

### TESTS DE DECOUPE DE MOTIFS 

Afin de me rendre compte de l'efficacité de cette technique, j'ai voulu tester les différents motifs trouvés en ligne sur une planche de bois de 3mm qui était mise à disposition dans les chutes du FabLab.  Pour cela je me suis servi du PDF mis à disposition sur le site **Trotec**.

![MOTIFS](images/kerfing.JPG)

### DECOUPE LASER

Je me suis servi du test d'échantillonage disponible au FabLab pour déjà savoir ma puissance et vitesse de découpe. Ainsi, vous pouvez voir les valeurs que j'ai choisi. Il ne reste plus qu'à attendre le résultat pour voir ce que ça donne. J'ai fais ça sur une planche de 120cm x 25cm.

![échantillonage](images/kerf.jpg)

### RESULTATS DE LA DECOUPE

Pour une première expérience, ce n'est pas trop mal mais toutes mes découpes n'ont pas été faites assez profondes. En tout j'ai réalisé 7 motifs différents sur la planche pour tester leurs réactions à la courbure. La découpe a pris beaucoup de temps (50 minutes) car le laser était à une vitesse moyennement élevée et il y avait beaucoup de motif. Mais l'avantage c'est que la planche produit très peu de fumée donc l'attente lors de l'évacuation de la fumée après la découpe est très courte. Grâce à ces tests j'ai pu comprendre un peu mieux le fonctionnement du Kerfing. 

![découpe résultats](images/cut1.jpg)

### PROBLEMES RENCONTRES 

Voici les résultats des découpes une fois retirées de la planche, comme on peut le voir, la plupart des découpes simples ont marché, mais d'autres non. Pour expliquer un peu mieux ce problème de découpe, il faut savoir que je me suis basée sur l'échantillonage disponible au FabLab. J'ai réglé la vitesse et la puissance du laser par rapport à ce dernier mais le résultat n'a pas été le même sur ma planche (pourtant de la même épaisseur et du même matériaux). Après réflexion, je pense qu'il s'agit d'un problème de vitesse, celle-ci étant trop élevée, le laser coupe bien au début mais plus le laser prend de la vitesse, moins le rayon de ce dernier n'a pas le temps de traverser l'entièreté du matériau. J'ai dû remettre un coup de cutter pour la plupart des découpes afin de retirer les chutes. J'ai déduit cela en regardant l'arrière des découpes : plus les découpes peuvent être faites rapidement, moins elles ont traversées le bois. Soit il faut diminuer la vitesse, ou augmenter la puissance du laser, soit il faut diminuer la taille du matériau.

![découpe épaisseur](images/cut3.jpg)

Au final, avec ce test, je remarque malgré l'imperfection des découpes, que la réaction à la courbure est plus favorable sur les deux motifs ci-dessous. Même si le deuxième test n'est pas entièrement évidé, le bois est déjà en apparence très souple. Dans le cas d'un matériaux aussi épais, c'est important d'avoir des motifs très ouverts mais pas trop de manière à ne pas faire casser le bois.

![réaction à la courbure](images/cut4.jpg)

### OMBRE ET LUMIERE 

Les effets d'ombre et de lumière provoquées par ce genre de motif sont super intéressants, l'idée que ma lampe soit modulable serait super chouette avec ce genre de découpes. On pourrait vraiment jouer sur les ombres et la superposition, ce qui était mon idée de base lors de mes premiers tests en polypropylène. Plus l'ombre projetée est éloignée de la lampe, moins on devine les formes des percements. 

![ombre lumière](images/cut5.jpg)

### ESSAIES SUR BOIS

J'ai repris ces deux motifs intéressants sur **Illustrator** et je les ai adapté aux dimensions de mes arcs de manière à pouvoir tester différentes échelles du motif et la réaction du matériau par rapport à la courbure à l'échelle. Pour cela, j'ai simplement agrandi le motif. J'ai voulu à chaque fois tester deux échelles différentes mais la découpe ne s'est pas passée comme je l'aurais souhaité. Ci-dessous les 4 dessins prévus avec deux échelles et deux motifs différents. Puis en dessous, le résultat de ce qu'il s'est passé lors de mes deux relances de tentatives de découpe. Pour précision, sachez que j'ai utilisé du bois multiplex 3mm là aussi. 

![illustrator](images/555.JPG)

![Nouvelle tentative](images/cut2.jpg)

J'ai mis les motifs intérieurs à F 600 et 65% puis 300 et 65% (toujours en m'aidant de l'échantillonage du FabLab) et les contours F 600 et % 100. Sachant que mon premier test de motif était à F 600 et 40%. Ainsi j'imaginais obtenir moins de vitesse et plus de puissance laser mais le résultat c'est avéré dans les deux cas beaucoup trop fort pour mon bois, qui la première fois fumait très peu, mais qui, ce coup-ci, fumait tellement que la fumée sortait par les côtés de la machine sans avoir le temps d'être aspirée. J'ai dû mettre ma découpe en pause plusieurs fois pour désenfumer la machine afin de ne pas l'abimer. Malgré cela, je n'ai pas préféré aller au bout de mes découpes, bien trop puissantes pour le bois choisi. En plus de cela, la découpe était si forte que le bois avait tendance à bruler un peu et des dépôts dû à l'épaisse fumée (comme on peut le voir sur la photo) se sont fait. Je n'ai donc à ce jour, pas encore trouvé le bon réglage et personne n'était disponible pour me conseiller ce jour là. Je reviendrais donc faire de nouvelles tentatives plus tard.

UPDATE : Il serait préférable dans ce cas de faire deux passes pour pouvoir couper le bois sans risquer ce genre de réaction : une première fois avec une puissance assez faible, et sans bouger la planche de matériaux, relancer la même découpe sur les traits déjà laissés par la première passe pour éviter une fumée trop dense et un bois qui brule. Au passage, après renseignement, il est préférable de ne jamais utiliser de MDF (plus souple et plus fin) au FabLab, je vais alors devoir trouver la bonne technique pour le bois, ou utiliser du carton gris.

### ESSAIES SUR CARTON

Afin de faciliter mes recherches pour le moment, je réitère mes essaies en utilisant ce coup-ci du carton gris épais de 3mm : une épaisseur qui me garanti une bonne rigidité avant découpe. J'ai fait évoluer mes dessins aussi en fonction des motifs que j'ai trouvé afin de leur donner une forme plus simplifiée (vue sur un autre site de FabLab) et de les étirer légèrement afin que la forme corresponde à mes attentes. On obtient donc le dessin SVG suivant : 

![dessin svg](images/dessin_01.12.2020.JPG)

J'ai une fois de plus fait uniquement des tentatives sur les deux premiers arcs afin de voir leurs réactions au **KERFING**, au pliage et vis-à-vis de leur matérialité. En fonction des résultats je poursuivrai mes découpes sur toute la lampe. Après une attente de bien 70 minutes (découpe de petits motifs à faible vitesse + désenfumage), j'ai enfin mes 4 arcs et je peux tester leurs réactions à la découpe. 

![découpes](images/découpe1.jpg)

Pour les réglages (pas bien visibles sur mon image), j'ai utilisé F800 et %65 sur la première passe (rouge) correspondant aux motifs intérieurs et F800 et %100 sur la seconde passe (bleue) qui correspond aux contours. J'ai trouvé la découpe un peu forte car elle fumait plus que ne devrait fumer le carton (ou trop lente), mais en voyant le résultat je ne pense pas que cela ai été trop fort : le carton est à peine "noircit" à quelques endroits où le laser a trop chauffé, mais ils sont rares et les motifs sinusoidaux était parfois même légèrement mal coupé à l'arrière. Cela est dû à l'épaisseur du carton. Globalement je m'en suis bien sorti avec l'aide dû nuancier pour les réglages ce coup-ci.

![découpes2](images/découpe2.jpg)

Comme vous pouvez le remarquer, au niveau du résultat c'est bien au delà de mes attentes, voir même trop. Le carton est devenu hyper flexible, il n'y a plus aucune résistance à la courbure en particulier pour la forme sinusoidale. Cette dernière peut se plier dans tous les sens, et même sur l'axe Z (ce qui n'est pas ma volonté au contraire). Le motif plus rigide en H est donc plus adéquat pour éviter la pliure X,Y,Z mais garantir une courbure du X,Y uniquement. Par contre je suis quand même épatée par la capacité d'un matériau rigide à devenir souple par simple évidemment géométrique répété.

Après les conseils de Victor, notre professeur, je devrais m'adresser à Denis Terwagne, afin de mieux comprendre comment dessiner mon propre motif correspondant à mes attentes par rapport à mon objet précisément. On parle de **METAMATERIAUX** dans ce cas. Wikipédia nous en donne la définition suivante : > En physique, en électromagnétisme, le terme métamatériau désigne un matériau composite artificiel qui présente des propriétés électromagnétiques qu'on ne retrouve pas dans un matériau naturel.

Pour le moment, je pense déjà avoir compris quelques grandes idées avec mes différents tests d'échelles et de motifs. Par exemple, les formes souples (en vague sinusoidale) aide à mieux courber un solide mais dans tous les sens d'axe possible, tandis-que les formes plus rigide (H) garantissent une courbure du matériau selon 2 axes uniquement. Plus les percements du motifs sont grands, plus le matériau est flexible mais cassant. Au contraire, quand les percements sont fins, le matériaux est plus rigide, par contre face à une sollicitation supplémentaire, il n'est pas fait pour plier et casse. L'épaisseur du matériau joue aussi beaucoup sur le motif à choisir, où en tout cas son échelle par rapport au degrés de courbure. 

### FILETAGE ET ROTATION

### LES TIGES FILETEES

Grâce à un [tuto youtube en Français](https://www.youtube.com/watch?v=pxneNgbq7Pk) très bien expliqué, j'ai pu comprendre comment réaliser mes propres tiges filetées. Cela me permet de créer mon propre système de rotation des arcs de ma lampe sur mesure sans avoir besoin de me rendre dans un magasin de bricolage pour acheter des pièces. Bien entendu, je n'ai pas suivi le tutoriel à la lettre, mais je l'ai adapté à mes besoins par rapport à ma lampe. En particulier pour le système de serrage. Rappelez vous dans le module 2, j'avais essayé de comprendre comment fonctionnait la rotation : 

![détails](images/détails.jpg)

![détails 2](images/dessin.jpg)

### MODELISATION FUSION 360

Avec l'aide du tutoriel, ce fut super simple : voici les étapes que j'ai suivi pour en arriver là où je voulais : 

Dans un premier temps j'ai déjà réalisé la vis. Pour cela, il a fallu conserver un espace de rotation lisse, le filetage ne devait donc pas faire l'entièreté de la vis. Ce filetage devait se visser dans un écrou en forme de sphère si on suit le modèle de la lampe. La vis devait pouvoir se glisser dans les percements des arcs (10mm de diamètre), je l'ai donc conçue avec un diamètre de 8mm pour garder une marge. La tête de la vis se devait alors d'être plus large que les percements pour ne pas traverser les arcs. 

![vis](images/vis1.jpg)

- 1 : créer un cercle de diamètre 8mm (comme expliqué ci-dessus), de préférence, avec un centre basé sur l'origine de la page **Fusion360**.
- 2 : extruder le cercle pour donner la longueur de la vis : j'ai choisi 6mm pour avoir une marge d'erreur (3cm de partie fileté, 2cm d'espace de rotation lisse et 1cm pour la tête de la vis). 
- 3 : créer un filetage sur la partie basse de la vis (dans solide > créer > filetage). Programmer la longueur du filetage, la taille de celui-ci, ect après avoir séléctionné les côtés de la vis sur lesquels on veut appliquer le filetage. 

J'ai ensuite réalisé l'écrou (qui adopte la forme d'une sphère). Pour cela, j'ai masqué le corps vis et travaillé également avec l'origine de la feuille :

![écrou](images/vis2.jpg)

- 4 : J'ai créé une sphère dont le centre est l'origine. J'ai choisi de faire une sphère de 60mm de diamètre avec l'outil solide > créer > sphère. 
- 5 : j'ai ensuite dessiné un cercle au centre de la sphère, de la même taille de la vis, au même emplacement que celle-ci (de 8mm donc).
- 6 : Après, j'ai extrudé le cercle dans la sphère pour creuser celle-ci et laissé l'emplacement pour glisser la vis sur 30 mm. 

Après, Avoir fait la forme de base de mon écrou, j'ai réalisé et perfectionné mon filetage :

![filetage écrou](images/vis3.jpg)

- 7 : De la même manière que pour la vis, j'ai ajouté un filetage à l'intérieur de mon écrou sphère (solide > créer > filetage). Grâce à la fonction mémoire cochée lors de la création du filetage de la vis. J'ai juste eu à cliquer sur le cylindre de mon trou dans l'écrou et le même filetage s'est appliqué à mon écrou. 
- 8 : Mon écrou étant fini, j'ai ajouté un petit détail en révélant ma vis : Je lui est ajouté une tête un peu plus large que la taille de mes percements d'arc (13mm de diamètre).
- 9 : Ma vis et mon écrou étant terminé, je vérifie que tout vas bien en cliquant sur Inspecter > Analyse de section. Ensuite je selectionne la face "xy" de manière à pouvoir avoir une coupe de mes objets en leur centre. Je peux ainsi constater que ma vis s'emboite bien dans mon écrou. C'est le cas ! 

Enfin, il faut savoir que la marge de jeux standard de sécurité qui existe entre les deux est très faible pour une impression 3D. C'est une marge calculée selon les standards des diamètres de nos vis existantes. Afin d'augmenter la taille de ce jeu (car l'impression 3D ne sera pas parfaite et on veut garantir le vissage), il va falloir réduire l'épaisseur du filetage de la vis ou augmenter l'épaisseur du filetage de l'écrou au choix. 

![augmentation du jeu](images/vis4.jpg)

- 10 : Pour cela on utilise l'option Solide > Modifier > Appuyer ou tier (raccourci Q). Les modifications dépendront de la géométrie séléctionnée. Je choisi de travailler sur la vis : je selectionne la tranche la plus extérieure du filetage puis je la décalle de -0,20 mm pour assurer le bon vissage.
- 11 : Ensuite je fais la même chose avec le dessus de la face du filetage mais de -0.10 mm.
- 12 : Enfin je fais pareil avec la face inférieure du filetage de -0.10 mm également. On obtient alors une vis qui a un plus grand jeux pour garantir l'emboitage des deux éléments après impression. 

Voilà ! Si vous avez suivi les étapes, vous avez-vous aussi fait une tige filetée avec un écrou sphérique. C'est très simple de l'adapter à la forme que vous souhaitez, surtout en s'aidant de la vidéo. Maintenant, il faut déplacer les deux éléments de manière à pouvoir les déposer séparément sur le plateau d'impression 3D et lancer l'impression 3D.

### REGLAGES DE LA MACHINE 

Pour faire mes réglages, il a fallu imaginer les éléments comme conçu pour une impression 3D et en tant qu'objets imités. Pour cela, il a été nécessaire de faire des réglages propres à mon modèle afin de poursuivre ma logique d'impression, économiser de la matière, du temps, de l'énergie et donc faire du sur-mesure en profitant des bénéfices que nous apporte la machine (les possibilités supplémentaires qu'elle apporte). Voici les explications et les réglages apportés à ma modélisation **Fusion360** sur **PrusaSlicer** afin de pouvoir imprimer les quatre éléments modélisés : 

![couches et périmètres](images/a.JPG)

Au niveau des couches, les réglages restent standards. Je mets une hauteur de couche identique sur toutes les couches, cela est préférable. Je modifie mon périmètre et mes couches solides sur 3 pour que mes couches soient proportionnelles verticalement et horizontalement. Et enfin je coche la case qui détecte les périmètres en faisant des ponts. Mes couches et mes périmètres sont alors bien programmés, je passe à la suite.

![remplissage](images/b.JPG)

Pour le remplissage, je mets un motif triangle pour que le temps de remplissage soit plus rapide mais surtout pour économiser de la matière car mon matériaux ne nécessite pas une résistance à de grandes forces. Ce ne sera qu'un simple boulon et on n'y touchera plus une fois mis en place, il ne nécessite donc clairement pas autant de matière qu'un vrai boulon, mais fait office de fermeture de ma tige filetée/axe de rotation. Pour le reste, je ne touche à rien, les paramètres par défauts me suffisent parfaitement concernant mon modèle.

![Jupe et bordure](images/c.JPG)

Pour la jupe, il faut maintenant penser un peu plus loin et adapter plus sérieusement les réglages à mon cas. La jupe dépend en faite de la disposition de mes objets. Les poser à l'horizontal n'aurait aucun sens par rapport à leur forme. Le but est d'avoir le meilleur impact au plateau de la machine (niveau solidité), mais le moins de perte de matière possible. Coucher mes objets reviedrais à faire des pont partout, cela n'aurait aucun sens. C'est pourquoi j'ai mis mes vis la tête reposant sur le plateau, et mes boulons le trou de vissage vers le haut (pour ne pas avoir de remplissage). Cela nous donne une forme qui, de prime abord, ne semble pas la plus logique puisque j'ai une grande hauteur (6cm). Toutefois, c'est la position qui demande le moins de matière et le plus de logique pour la machine (moins d'allez retour, gain de temps et d'energie). Cela se comprendra d'avantage lors de l'explication des renforts. Mais pour l'heure, parlons "jupe" : J'ai allongé ma bordure d'une longeur de 15 mm afin de garantir à la vis et au écrous une bonne prise au plateau et qu'ils ne tombent pas durant l'impression. Cela ne prend pas énormément de temps à faire en plus, et très peu de matière.

![supports](images/d.JPG)

Maintenant, pour mieux comprendre ce que j'expliquais avant, je vais déjà vous dire mes réglages concernant les supports et vous expliquer pourquoi. J'ai coché la case "générer des supports" mais décoché celle "généré automatiquement". On pourrait se demander pourquoi ? mais cela s'explique par le fait que la machine génère par défaut des supports moyennement utiles mais présents par sécurité dans le percement de mes boulons. Or, je ne saurais retirer ses supports. Je demande donc au logiciel de ne pas les générer automatiquement mais de me laisser choisir. Cela me permettra d'en mettre seulement sur le bas de mes éléments au endroits nécéssitants sans obstruer la fonction de mon objet. Ainsi cela explique pourquoi j'ai généré des supports sur les "50 premières couches". Afin de stabiliser mes éléments. Toutefois il restait quelque chose qui m'ennuyait : les sphères déposées à même le plateau sont souvent abimées au niveau du point de contact parce que les supports fusionne avec la courbe de celle-ci. Je me suis alors demandée si sur-élever mes sphère n'était pas une bonne solution afin que celles-ci ne reposent que sur des supports et ne gardent pas de traces de la jonction plateau (aspect moins lisse et uniforme). J'ai donc tenté cela en ajoutant "7 couches au radeau". Enfin, pour être cohérente, j'ai cochée "générer des supports sur le plateau uniquement" afin qu'il n'y en est pas dans des trous impossibles à évider.

![filament](images/e.JPG)

Ces réglages m'ont permi d'obtenir le dessin suivant : peu de supports mais assez pour de belles pièces, rapidité, économie de matière, et solidité. J'ai donc profité des avantages de la machine pour obtenir un résultat sur mesure correspondant parfaitement à mes attentes. Je peux désormais lancer mon impression.

### IMPRESSION 3D

Voila, mon impression est lancée, je la laisse à une vitesse de 100 %, c'est plus sur, sinon le pas de vis risque de ne pas être bien. Pour le moment, le début est prometteur, je n'ai pas encore récupéré la pièce mais la fixation au plateau est solide et l'impression est très rapide : 6H03 ! C'est très peu pour une impression 3D, surtout en comparaison à mes premières impressions qui dépassaient les 13 - 15H facilement ! 

![impression en cours](images/imprimante_jaune.jpg)

### RESULTATS

Je suis super déçu, malgré mes calculs, l'impression a échoué vers la fin quand je n'étais plus là pour la superviser. Cela s'explique par la chute d'une de mes vis sur l'autre. Normalement cela n'aurait pas du se produire, alors soit quelqu'un a toucher à ma vis en cours d'impression, soit je me suis trompée dans mes calculs. Quoi qu'il en soit, peu importe, même si la vis ne va pas jusqu'au fond de mes écrous, une grosse partie de celle-ci s'enfonce dedans et c'est largement suffisant avec le filetage restant pour que ça tienne. Pour le final je ferai en sorte de réimprimer mon axe de rotation mieux mais pour les tests c'est déjà un bon moyen de vérifier le principe. En tout cas, j'ai la possibilité de couper la partie de la vis mal imprimée (cf photo ci-dessous) et de constater que mes paramètres et mon pas de vis étaient parfait ! Le jeu ajouté entre la vis et l'écrou permet un vissage idéal au mm près ! 

![résultats](images/visrésult.jpg)

Si on ajoute mon axe de rotation aux découpes laser tests, on peut voir que tout marche parfaitement comme imaginé ! Effectivement l'axe n'est pas callé parce que le nombre d'arcs finaux n'est pas complet, il y a donc du jeu sur la zonne de rotation, mais les résultats lumière et rotation sont concluant. Il ne me restera plus qu'à réimprimer cela correctement. Cela dit je trouve la boule peut-être un poil trop grosse pour le module à l'échelle 1/2. Je penserais peut etre à la réduire, ca me fera gagner encore plus de temps et de matière. 

![montage](images/testboule.jpg)


__________________________________________

## PHASE 3 : EVIDER LA MATIERE

Après une concertation avec les professeurs et accompagnants du projet, je comprend que je m'impose bien trop de contrainte à travailler sur le bois et à vouloir modifier ses caractéristiques. En effet travailler avec du bois demande beaucoup de temps pour cerner les réglages qui conviennent, la découpe est particulièrement longue donc la production d'un tel objet sera couteuse et longue. De plus, le fait de vouloir rendre le bois pliable impose un motif qui ne permet plus autant de liberté au niveau des percements et des effet de lumière. De plus le matériau opaque offre de bon effet d'ombre, mais ne permet pas de jouer sur la superposition et les effets translucides d'une lampe conventionnel (au contraire de mon premier test qui était plus concluant lorsque j'utilisais le polypropylène). En plus de cela, je m'éloigne de mon influence de base qui était Vasarely en essayant d'atteindre la performance souhaité du bois. 

### MIXE DES DEUX IDEES

Pour continer d'avancer sur mon projet, il va falloir que je mixe mes deux idées : jouer sur les percements, conserver la modularité et obtenir des effets visuels. Je decide de revenir à mon matériau premier dont les caractéristiques permettent directement au polypropylène d'adopter la forme que je souhaite et de tenir cette forme ! De plus le matériaux est bien léger et permet dès lors d'avoir une meilleure tenue à la forme déployée sans la contrainte de l'apesanteur. 

### FORME INFLUENCEE PAR VASARELY 

Pour revenir sur une forme influencée par **Vasarely**, je vais devoir me replonger dans son univers, je m'en suis trop éloignée. Pour cela, j'analyse ses différentes productions et je cherche peut etre un motif plus simple et reproductible facilement en prennant en compte les critères des machines. Si je travaille sur des percements, je dois otper pour des formes évidées faisables. La modularité doit permettre de produire des effets cinétiques, donc un jeux de percements avec des perturbations optiques serait le bienvenue. En étudiant son travail d'un peu plus près, je découvre un gros travail de superposition super interessant où il joue justement sur des motifs qui se superposent de gabarit différents et sur différentes couches. C'est exactement ce que je cherche à faire avec ma lampe ! En images, quelques référentes qui illustrent bien mon propos : 

![refs](images/opart.jpg)

### CREATION DE MON MOTIF

J'ai du imaginer mon propre motif mais celui-ci devait etre une forme continue autre qu'un cercle ou un carré qui aurait évidé la totalité de la matière coupé. Je decide donc de mixer plusieurs des idées si dessus en étant très influencé en particulier par la dernière oeuvre où il reprend la forme du cercle à travers des lignes. Les courbes sont suggèrées mais pas continues. Il le fait sous forme de lignes droites difformes. Je pars donc sur cette idée en multipliant mes cercles.

![explication 1 bande couleur](images/expli1.jpg)

- 1 : La première étape pour faire cela est de comprendre comment faire cette forme avec des coutours vectoriser. Ce n'est pas simple en ayant une connaissance moyenne du logiciel **Illustrator** mais j'ai fini par parvenir à mes fins sans traits supplémentaires et de manière très rapide. Les explications arrives : D'abbord faire des lignes de guide à intervalle régulier pour avoir un écart logique (ou utiliser un quadrillage). Puis tracer l'arc de la lampe. Mettre des guides sur les zones où on veut dessiner des motifs, puis dessiner les motifs : des cercles, des lignes épaisses (intérieur du cercle) et d'autres plus fines (extérieur du cercle). Peut importe leur taille, on ajustera après, et travailler avec des couleurs de remplissage aide, on les modifiera par la suite pour enlever le remplissage. En gros par rapport à mon dessin les lignes jaunes vont former l'interieur du cercles noirs, les vertes vont être nos bandes évidées et les lignes de guides vont disparaitre.

![explication 2](images/expli2.jpg)

- 2 : On séléctionne toutes nos formes, puis avec l'outils "concepteur de forme" on vient glisser sur les zonnes à fusionner (en l'occurence, comme on le voit sur le dessin, sur les bandes peu épaisse dans les bandes intérieures du cercle). Ainsi, grâce à cet outil, on obtient une forme qui correspond à notre cercle et à notre grosse bande, la petite bande qui passait à l'intérieur ayant disparue. On utilisera toujours cet outil. 

![explication 3](images/expli3.jpg)

- 3 : Maintenant on sort de la selection et simplement en séléctionnant les bandes oranges restantes autour des cercles, on les efface. Il ne reste alors plus que nos cercles, nos grosses bandes et nos plus petites. Reste a supprimer le surplus de cercle.

![explication 4](images/expli4.jpg)

- 4 : On séléctionne toutes nos formes dessinée dans les arcs, et une fois de plus on active l'outils "concepteur de forme". On fait alors glisser la souris le long des interstices du cercle non évidé. Les bords du cercles inutiles disparaissent donc, laissant place à un vide de matière sans interruption. Il me semble qu'il est aussi possible de faire cela sans cet outil et simplement en découpant les portions de cercles avec les ciseaux aux intersections, mais je pense que cette méthode est bien plus longue. 

![explication 5](images/expli5.jpg)

- 5 : La dernière étape, on supprime les contours du cercles qui interrompe la forme entre les bandes épaisses intérieures et les bandes fines extérieurs. Cette action permet d'éliminer les découpes laser inutiles. Pour cela, toujours avec l'outil "concepteur de forme, on relie les deux bandes (fines et épaisses) en traversant le contour et ce dernier disparait. Puis on oublie pas de tout séléctionné pour effacer les remplissages par défaut et faire les contours d'une seule couleur afin de gérer ensuite les découpes laser sur **DriveBoardApp**. On obtient alors le dessin suivant en faisant varier la tailles des bandes et la dispositions des cercles : 

![global](images/66r.JPG)

### LA DECOUPE LASER

On peut lancer la découpe laser sur les plaques de polypropylène. Je met les réglages suivants : vitesse F de 1300 et puissance de 50%. J'applique la même valeur au deux couleurs mais je distingue bien les deux couleurs pour l'ordre des découpes : les motifs avant, le contour extérieur des arcs après, pour limiter le risque que les découpes ne bougent. On obtient alors ce résultat plutôt concluant ! 

![explication 6](images/expli6.jpg)

Au passage, on avait parlé de faire de la gravure plutôt que de la découpe pour être plus libre des forme avec Victor, notre professeur, mais en regardant l'échantillonage des découpes, je pense que ce n'est pas une bonne idée. Sur les plaques aussi fines et translucide de polypropylène, la gravure ne rend pas bien et n'est pas du tout visible (voir photo ci-dessous), d'où la redirection sur une forme découpée. 

![échantillonage](images/130736681_572988570217707_344506554752002094_n.jpg)

### AUTRE MATERIALITE

J'ai quand même envie d'essayé sur une autre matérialité, la découpe a été super rapide en comparaison à celle sur bois (20-30 minutes pour tout). Ce qui me dérange dans ce polypropylène c'est l'absence de teinte et la translucidité peut etre un peu trop élevée. J'ai flaché sur des papiers épais légèrement cartonnés qui se trouvait mis à notre disposition, ca peut donner un superbe effet de lumière si le orange se reflète comme il tire sur le fluo. Je prend donc un carton entier et une chute, je découpe mon modèle (car les dimensions sont plus petites) pour faire deux fichier et imprimer en deux fois, et je lance la découpe. Ci dessous mon paramètrage et mes résultats.

![résultat](images/resu2.jpg) 

J'aime beaucoup le résultat, à voir comment la lumière réagit, mais la couleur et le motif vont super bien ensemble, après l'effet n'est pas le même mais ça peut etre chouette si la lumière ce reflète bien ! 

### IMPRESSION 3D

Au passage j'ai relancé une impression 3D, car la taille de la boule servant d'écrou ne me satisfaisait pas (trop grosse), et les vis étaient trop grandes et ont fini par tomber avant la fin de l'impression 3D, même si mon système fonctionnait toujours, il n'était pas au point. Pas besoin de tout changer pour cela, il m'a suffit d'aller dans mon fichier **Fusion 360** et de revenir sur une ancienne étape de ma modélisation juste pour sous dimensionner la taille de ma boule (la passer d'un diamètre 60 mm à 40 mm). Ainsi pas besoin de tout modifier, changer cette étape affecte toutes les autres étapes sans tout remodifier. C'est l'avantage de fusion360 ! J'ai aussi abaissé la hauteur de ma vis de 2cm pour limiter le risque de tomber lors de l'impression, d'autant plus que le diamètre de ma boule était plus petit désormais donc moins de partie fileté dedans !  

![boule explication](images/expli_7.jpg)

L'impression c'est parfaitement passé, aucune casse et la rotation se fait parfaitement dans les vis ! C'est top, j'ai mis les même paramêtres que pour les premières et cela n'a pris que 3H10 d'impression. Par contre il ne restait plus que la machine avec une buse en 0.6 mm ! Il a donc fallut modifier les paramètres de l'imprimante en mettant 0.6 à la place de 0.4mm et aussi mettre des hauteurs de couche à 0.2 et non 0.3 mm. Sinon tout a fonctionner et les fermetures en forme de boule sont bien plus proportionnelles avec un diamètre de 40 mm que 60 mm ! 

![boules impression](images/boules.jpg)

 ### RESULTATS EN LUMIERE 

- **polypropylène transparent** : le résultat est sympa, bien quE cela confirme mes craintes : un peu trop transparent. Mais la rotation autour de l'axe se fait parfaitement, la matière se tient et les arcs sont bien en compression autour de l'axe de rotation qui fonctionne en traction. La lumière produite est un peu froide, surtout avec l'ampoule de la lampe de travail. Il faudra penser a jouer sur des ambiances chaudes ou colorées si je viens à partir sur ce choix afin que ma lampe devienne une lampe d'ambiance.

![translucide](images/lumiere0.jpg)

- **papier cartonné orange** : Les arcs se tiennent bien aussi, la rotation pareil. Je trouve le rendu bien plus interessant avec les percements de lumière qui ressortent davantage. En plus le orange apporte une touche de lumière chaude qui est très agréable. Le orange va par contre terriblement mal avec les axes de rotation jaunes, mais bon ce ne sont pas mes couleurs finales. C'est le contraste qui accentue les effets de lumière. Les résultats ci-dessous : 

![orange](images/lumiere1.jpg)

Par contre un gros bémole vis-à-vis de la matière : elle se découpe super bien mais plie très mal, en partie sur les zones évidées, la courbe n'est pas homogène, c'était à prévoir mais bon ... Peut etre essayer avec un autre type de papier ? Ou du polypropylène teinté ? En plus le coté blanc prend très vite les traces de brulure du laser. Dommage parce que le contraste intérieur extérieur était top. 

![problème](images/lumière2.jpg)

### BONUS 

Petites vidéos des effets :  
![translucide](images/video-1607454087.mp4) ![orange](images/video-1607454360.mp4)

### CONCLUSION

- L'axe de rotation avec des fermetures plus petite c'est mieux (40mm de diamètre)
- Une matière plus souple c'est beaucoup plus simple et plus libre à travailler au niveau du motif
- Pour des bons effets de lumière, un contraste est préférable donc pas trop de transparence dans le matériau
- Le motif doit etre simple et pas trop découpé, sinon cela destructure les qualité de pliage du matériaux (trop souple)
- Préférer des couleurs chaleureuses ou une ampoule chaude/colorée


__________________________________________


## PHASE 4 : MOUVEMENT ET FILETAGE

### MISE AU POINT 

Avec la multitude de tests réalisés avant, j'ai pu exepérimenter des objets chouettes, mais quelque chose me gène pourtant toujours autant dans chacun d'eux. Je n'arrive pas à sortir de ma forme, je ne parviens pas à m'éloigner et revisiter ma lampe. La forme reste la même et même si je faisais 50 déclianaisons de plus, je pense que je ne parviendrais pas à m'éloigné de cet objet de base. Il est vrai que la lampe Gherpe de Superstudio m'a beaucoup séduite et désormais je n'arrive plus à aller plus loin de peur de la dénaturer. 

Après m'être concerté avec Victor, Helene et Gwen, il est clair que tout le monde est d'accord sur ce pas. Je pense que faire un pas en arrière et revenir au début de l'exercice m'aidera à y voir plus clair. Ainsi reclarifier mes intension me débloquera sans doute. 

![réinteprétation](images/mudar_0.jpg)

### INFLUENCE, POURQUOI ?

Comme je l'ai expliqué, j'ai choisi de travailler sur le mot **INFLUENCE** puisque ce mot induit qu'on se nourrisse de notre vécu pour produire un objet qui possède un rapport de cause à effet. C'est notre vécu, ce que nous avons vu au cours de notre vie, ce qui nous entourre qui influence nos choix et nous mène à penser une création d'une certaine manière plutôt qu'une autre. Chacun de nos gestes est influencé. 

J'aime penser que les choix que l'on fait au cours de notre vie n'est pas le fruit du hasard, mais au contraire, qu'ils sont guidés par le mélange de toutes les manifestations qui ont eu lieu autours de nous et nous ont influencées. Par exemple, notre éducation nous forge une certaine ligne de conduite, dont nous n'avons pas conscience, mais qui est induite et irrémédiablement intégrée en nous. Mais alors comment retravailler la lampe et étant bercée par les influences de ma vie ? Puis deja comment réinterpreter ma lampe tout court ? Parce que jusqu'ici je reste bloquée dans la reprise en essayant d'y intégré des références supplémentaires (Vasarely), mais je ne parviens pas à m'éloigner de mon objet. J'aimerai faire quelque chose de plus sincère, où j'y retrouve de ma personne.

### REINTERPRETER 

Mon problème c'est d'être fascinée par cette lampe et ma peur de la dénaturer. Alors prenons le taureau par les cornes, ré-analisons là et tentons de la déconstruire pour mieux réussir cet exercice. J'avais tendance à trop la prendre dans son ensemble mais concrètement ce qui m'a le plus interessée chez elle c'est clairement son système de rotation, alors pour essayé de me débloquer, je vais essayé de travailler la dessus. Ma réflexion à donné cela : 

![recherches](images/recherchesmudar.jpg)

En gros en décomposant la lampe et en prenant les pièces une par une, j'ai clairement vu du potentiel dans le système rotatif. A partir de sa forme mais surtout des capacité qu'il offrait cela m'a permi de développer mes idées. L'axe est fait d'un système fileté qui permet de resserer et désserer les objets. L'écrou de cette lampe n'est pas conventionnel, il est décoratif. Généralement considéré comme élément secondaire, on cherche souvent à masquer l'écrou sur un objet. Pourtant pour la lampe Gherpe, Superstudio a tenu à le designer et lui donner de l'importance. Il devient un élément décoratif. Il est présent et se distingue dans l'objet. Cela m'offre déjà une belle piste pour la suite : 

- Utiliser le système de rotation comme un élément en soit à réinterpréter
- détourné l'élément décoratif de cet axe pour en faire la partie principale de l'objet. 

En fait, c'est en redessinant le système interessant de la lampe que l'idée m'est venue. Si on reste sur l'idée de la lampe, et si le système fileté était agrandit au point qu'il devienne support, l'écrou (boule) deviendrait ma lampe. Mais, exploiter cette forme telle qu'elle ne serait pas intéressant, il faut voir plus loin : plutot que de reproduire la boule avec un matériau solide comme le PLA à la machine 3D, pourquoi ne pas jouer sur un objet flexible ? 

De là m'est alors venu l'idée : je ne devait pas produire une boule, mais décomposer cette boule elle même pour voir comment la faire en matériel souple. La solution évidente : des arcs courbés ! Et voilà, on en revient à nos arcs de la lampe Gherpe ! Même matériaux, même principe, mais la forme est totalement différente et la modularité que proposait la lampe est amplifié puisque là on joue sur la forme des arcs ! 

En retravaillant la lampe Gherpe, j'avais du mal au début à sortir de l'objet car il m'était impossible de modifier sa forme : changer les arcs aurait bloqué le système de rotation, et la lampe aurait perdu de l'interet. Mais là c'est l'occasion de proposer une lampe qui puisse avoir des **arcs modulables** (déployables ou pas, mais aussi modifiables dans la forme). Le fait de travailler avec une tige fileté permet de faire varier la hauteur des arcs et donc leurs courbures. 

### MODELISATION FUSION 360 DU SYSTEME

Ainsi si la tige filetée devient le luminaire, elle se doit d'etre transparente pour maintenir et diffuser la lumière (néon ou led) en son sein. Elle permet aussi de se fixer au pied de la lampe (socle) grâce au filetage. Je commence alors la modélisation de mon idée sur **Fusion 360** : 

**Le pied de la lampe:**

![pieds](images/mudar_1.jpg)

- 1 : je fais un pied de 10 cm de diamètre avec un second cercle inscrit de 2 cm qui servira à visser la tige filetée. 
- 2 : J'extrude le cercle extérieur pour donner de l'épaisseur au socle (1,5cm). 

**La tige filetée**

![tige](images/mudar_2.jpg)

- 3 : Je trace un cercle de 2 cm pour créer la tige filetée
- 4 : Je l'extrude sur une hauteur de 20 cm (taille limite du plateau de l'impression 3D)
- 5 : Je crée ensuite un filetage de 20 x 1,5 mm tout autour de la tige. 

![tige](images/mudar_3.jpg)

- 6 : Au centre de la tige filetée, je supprime la matière pour glisser ma source lumineuse, pour cela je choisi de supprimer un rond de 17,5 mm que je trace au centre. 
- 7 : J'extrude ensuite cette partie pour évider la pièce. 
- 8 : Je pense enfin à rajouter du filetage à l'intérieur de mon socle pour pouvoir visser la tige dessus (20 x 1,5mm).

**Ajustements filetage**

![filetage](images/mudar_4.jpg)

- 9 : Pour améliorer le filetage du socle lors de l'impression 3D et garantir son fonctionnement, on ajoute du jeu supplémentaire à la pièce comme on l'avait fait pour les premiers filetages. On décalle d'abord la zone inférieure de -0.1 mm avec l'option Solide > Modifier > Appuyer ou tier (raccourci Q) comme expliqué dans mes premières expérimentations de filetage .
- 10 : Puis la même chose avec la zone inférieure (-0.1 mm). 
- 11 : et enfin, pareil avec la partie centrale du filetage du socle mais de -0.2mm.

**Les écrous**

![filetage](images/mudar_5.jpg)

- 12 : On trace deux cercles : un de 3 cm de diamètre et un second inscrit au centre de 2cm de diamètre. 
- 13 : On extrude le cercle extérieur de 4 millimètres (ils ne doivent pas être trop épais pour ne pas géner visuellement mais doivent etre suffisament solide et grand pour avoir un filetage interne nécessaire au vissage). 

![filetage](images/mudar_6.jpg)

- 14 : On ajoute un filetage identique à celui de la tige fileté de 20 x 1.5 mm à l'intérieur de l'écrou.
- 15 / 16 / 17 : On ajoute du jeu supplémentaire au filetage pour garantir son bon vissage sur la tige fileté comme pour le socle. 

**Finitions socle** 

![filetage](images/mudar_7.jpg)

- oui j'ai sauté involontairement le numéro 18.
- 19 : pour pouvoir faire paser la gaine de la lumière, il faut un espace suffisant dans le socle : Pour cela, on trace un cercle de 1,5 cm de diamètre dont le centre se trouve sur sur le bas du socle. On obtient alors un demi cercle sur la partie socle de 0,75 cm dessiné sur notre socle.
- 20 : on extrude le demi cercle pour évider la matière entre le centre du socle et l'extérieur de celui-ci. 

**Forme finale**

![filetage](images/mudar_8.jpg)

- 21 : On duplique ensuite les écrous de serrage pour en obtenir 4. 
- 22 : Et voilà, nos 5 éléments finaux sont présents et bien modélisé. 

### ARCS EN DECOUPE LASER

![arcs](images/ARCS.JPG)

Pour les arcs je choisi pour le moment de faire des découpes simples qui viendront s'enfiller autour de la tige filetée. Quelques soit leur forme, il faut qu'ils fassent au moins 250 mm de long et que les percements soient de 20 mm pour se glisser autours de la tige filetée. 
Pour les découper, on utilisera les réglages les plus efficaces et rapides sur une plaque de polypropylène inférieure à 500 g/m² : F 1300 et 50 %. 

### APPERCU DU RENDU

![planche1](images/Sans_titre-1.jpg)

Pour le nom de la lampe, je choisi un nom qui rappel sa fonction première : **MUDAR**, ce mot, en espagnol, signifie (quand on parle d'un objet) changer / modifier / bouger quelque chose. Il évoque le mouvement et l'impermanance de la forme. La mutation de la lampe et le caractère évolutif de celle-ci est bien retranscrit je trouve à travers ce mot. En plus de ça, si vous ne l'aviez pas remarqué encore, j'aime bouger, faire milles choses, découvrir de nouvelles choses constamment. J'ai un côté un peu hyper-active et je crois que cette lampe en **mouvement**, c'est un peu une **part de moi** : Elle n'en fini jamais de ses **possibilités inépuisables**. On peut la changer à l'infini, on ne s'en lasse pas, sa forme est mouvante, adaptée à toute situations. Finalement je n'avais pas besoin de chercher des références et tomber dans une déclinaison à la limite de l'hommage, mais simplement de m'écouter. Mon influence à moi ce n'est pas une chose, mais c'est ma vie entière vécue. C'est une **infinité de possibilités** qui se sont ouvertes à moi et qui, à travers cette réinterprétation, agite ma réalisation. Ma lampe n'est pas une simple lampe d'ambiance, elle change, évolue, s'adapte, elle **prend vie**. Et là je retrouve l'enthousiasme et l'angouement que j'avais lors de ma découverte de la lampe Gherpe, mais avec une réinterprétation totalement différente mais très liée.

![planche2](images/Sans_titre-12.jpg)

**DO IT YOURSELF!** c'est l'idée qu'au dela de la modularité, cette lampe propose aussi une personnalisation en fonction des ambiances souhaitées.Elle propose une forme de base qui peut être modifiée par quiconque se la procure, son design est simple et reproductible. Ainsi quiconque peut reproduire les arcs sur du polypropylène d'une teinte différente ou "pimper" les écrous en les imprimant de la couleur de son choix. Ainsi la lampe peut avoir différents aspects à la manière des lampes **Fatboy** ou des chaussures **Nike** personnalisables:

![nikes](images/nikess.jpg)

L'objet est proposé dans un design simple et plutôt neutre, et chacun est libre de choisir de le personnaliser en fonction de ses attentes et de l'usage qu'il en fera (dans mon cas : la lampe d'ambiance peut adopter plusieurs couleurs qui peuvent correspondre à l'ambiance voulue).

**FACILITE DE MONTAGE ET GESTION D'AMBIANCE** 

![planche3](images/Sans_titre-13.jpg)

Le montage est simple et tout est modulable ! La disposition des arcs autours de l'axe lumineux dépend de l'intensité lumineuse souhaité, la courbure de ces arcs varient en déplacant les écrous, pour changer les arcs il suffit de dévisser les écrous. 

### IMPRESSION 3D 

Un problème auquel je n'avais pas pensé : ma tige filetée ne peut pas avoir de supports car cela abimerait le filetage extérieur. Elle ne peut pas non plus avoir des renfort qu'à l'intérieur car **PrusaSlicer** ne permet pas cela. J'ai donc du découper ma forme en deux sur **Fusion360** pour l'imprimer couchée. Pour cela j'ai dupliqué la forme et ai extrudé à chaque fois un coté afin de garantir un filetage continu. J'ai également du agrandit l'épaisseur des paroies pour obtenir des paroies de 2,5mm (soit un tube de 20mm avec une extrusion intérieure de 15 mm sinon la machine ne comprenait pas bien les formes et avait tendance à la traverser avec des renforts. 

![tige coupée](images/impress_0.jpg)

- Pour la partie socle et écrous, pas de soucis. Les réglages sont les mêmes que d'habitude, standard. 

![PET](images/pet.jpg)

- Pour la partie tige filetée, j'ai demandé à utiliser un filament transparent afin que ma lumière se diffuse. Malheureusement le seul filament transparent que possedait le FabLab n'était pas du PLA mais du PET. Avec l'aide de Gwen, j'ai donc du adapter mes réglages d'impression. Nous avons fait beaucoup de tests avant de réussir à lancer une impression qui fonctione (encore faut il voir le résultat). Au final en passant l'après-midi dessus nous pensons avoir les bons réglages : 

![paramètres1](images/impress1.JPG)

En fait ce qui a été le plus compliqué, c'est de trouver la bonne température de plateau et d'extrudeur, en y allant par tests multiples et en regardant sur internet, nous avons finalement décidé de mettre 240°C pour l'extrudeur et 85°C pour le plateau. Et, aux dernières nouvelles cela marchait. Ensuite, les réglages sont plus classiques : 

![paramètres2](images/impress2.JPG)

Mettre une hauteur de couche identique entre la première et les autres (0.2mm) et équilibrer les couches des coques horizontales (2 partout).

![paramètres3](images/impress3.JPG)

Pour les supports, on doit faire en sorte que ceux-ci se retirent facilement sans risque de briser la tige qui est la plus fine possible. On agrandit donc l'espace de ceux-ci en mettant tout à 4 partout (espacement du motif (mm), couche d'interface, espacement du motif d'interface(mm)). Et on coche la case pour générer les supports automatiquement. 

### RESULTATS

![lampe résultats](images/lampemudar1.jpg)

La tige filetée a marché !!! Elle n'est pas parfaitement transparente mais la lumière de ma bande led se diffuse bien. Il a suffit de la poncer un peu pour permettre aux écrous de coulisser plus facilement et je l'ai collée avec de la super glue. Les 10 arcs en polypropylène sont de la taille suffisante pour faire le tour et envelopper la totalité de la lampe. Les écrous font également bien leur travail pour fixer les arcs à une hauteur voulue. Evidemment pour les arcs il ne faut pas se fier à la couleur choisie qui n'est pas "merveilleuse" mais il me restait une plaque à découpé donc j'ai essayé pour mettre un peu de couleur dans mon modèle.

### REMARQUE A MODIFIER PAR LA SUITE

![lampe résultats2](images/lampemudar2.jpg)

- La taille de la lampe est satisfaisante, je ne voulais pas faire une grande lampe trop imposante mas rester discrète dans la mouvance de la lampe Gherpe.
- Les arcs ont une bonne forme et une bonne taille pour faire tout le tour de la lampe, mais comme ils sont tous égaux, ils ne peuvent pas se superposer ce qui est dommage. Il faudra revoir cette partie là.
- En plus ils sont encore trop fidèles à la lampe de référence mais avec un aventage de modularité en moins. 
- Les écrous et le socle blanc sont une bonne base pour une lampe blanche personnalisable. 

![lampe résultats3](images/lampemudar3.jpg)

- J'ai tenté de les faire d'une autre couleur pour voir le résultat (bon une fois de plus en orange -> seule bobine disponible, ce n'est pas le top mais ca donne une idée). Le modèle se reproduit rapidement (environ 3-4H en fonction de la vitesse), il n'y a pas de déchets car pas de supports, seule une petite jupe). 
- La tige filetée reste lumineuse sur toute la longueur, il faudra penser à la masque pour que les arcs colorés agissent comme des filtres intéressants car si même hors de arcs la tige s'illumine, les arcs colorés perdent de l'interet. 
- Il faudra aussi penser à une finition pour le haut de la tige (qui a noircit lors de l'impression et pour masquer l'intérieur de la tige), par exemple, une sorte d'écrou/bouchon. 

### PROBLEMES

Si on ajoute une impression opaque sur la tige, elle s'épaissit donc c'est moins esthétique. En plus on perd l'aspect modulaire des arcs "tout au long de la tige" puisque cela rviendrait à condamné une partie de la tige. Peut-être imprimer la tige en plusieurs couleurs ? En tout cas pour l'heure j'essais quand meme d'imprimer le bouchon/écrou voir le résultat mais je pense que ca ne va pas forcément me plaire. 

![lampe résultats3](images/lampemudar4.jpg)

J'imprime le tout avec des réglages bascis, sans supports pour limiter la matière, l'ouverture du bouchon vers le haut pour ne pas avoir un trop gros pont sur la fin du bouchon, qui là, repose sur le plateau de la machine 3D. 

![bouchon résults](images/bouchoun.jpg)

Les résultats ne sont pas satisfaisants. Le bouchon est trop gros, trop présent sur la lampe, en plus l'imprimante l'a un peu noircit et il ne masque pas la totalité de la lumière en plus en blanc mais l'épaissir plus serait vraiment laid. Il faudra plutot imaginer un filetage avec deux demis-cercles qui viennent se refermer à la fin du filetage qu'on viennent coller après, pour au moins ne plus voir l'intérieur de la tige. 

### MODIFICATION DES ARCS

Pour les arcs, plutôt que de rester sur la meme forme, rester trop ancré à ma référence, et ne jouer que sur l'ouverture ou la fermeture de ceux-ci, j'ai décidé de leur apporter plus d'interet : Qu'ils se plient ou se déploient de manière plus réfléchie que par simple effet de compression. J'ai parlé avec Gwen de tout ça et elle m'a dit que ça pourrait etre chouette de voir un peu les système de lampion en papier, voir leurs pliages. C'est effectivement ce que j'avais en tête, un système de pliage qui se déploie, ou en tout cas pas de simples arcs mais une vrai forme pour mes arcs.

![lampions](images/lampion.jpg)

Pour cela je me suis interessée au travail de découpe Laser de [Théo](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt/modules/module04/) qui m'a permi de découvrir d'avantage la méthode de [**pliage de Miura**](https://fr.wikipedia.org/wiki/Pliage_de_Miura) et diverses pattern similaires qui pourrait me permettre de vraiment jouer sur la forme déployée. 

> "Le pliage de Miura est une forme d'origami rigide, ce qui signifie que le pliage peut être effectué de façon qu'à chaque étape chaque parallélogramme est complètement plat. Cette propriété permet qu'on utilise cette technique de pliage pour plier des surfaces en matériaux rigides. Un pliage de Miura replié peut être rangé dans une forme compacte, son épaisseur ne dépendant que de celle du matériau à plier. Le pliage peut aussi être déployé par un seul mouvement en tirant sur l'extrémité opposée du matériel plié, et de la même façon on peut le plier à nouveau en poussant cette fois.".

![origami](images/miura.jpg)

Après une douce soirée, loin d'être ininteressante sur mon rapport à la patience et ma douceur, forte en pliage origami et en essais de tout genre (sur papier), me voilà arrivée à faire ce test (ci-dessous). Je l'ai dessiné sur **Illustrator** un peu au hasard en me basant simplement sur le périmètre et la hauteur de ma lampe. J'ai joué sur 3 couleurs (pour les gravures : pliage + les découpes) et un système pour le fixer à ma tige fileté par rapport au diamètre de ma lampe. J'ai choisi de déja faire un test sur polypropylène translucide avec un pattern de type diamond car très simple à réaliser, pour voir comment réagissait la matière. Pour les gravures je me suis basée sur le GitLab de [Théo](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt/modules/module04/) qui a deja tester les différents réglages de la laser destiné à cet usage. Cela m'a évité trop de tests et je commence à bien connaitre cette matière. 

![test](images/diamondtest.jpg)

Voici donc mon pattern imprimé. J'avais peur que le polycarbonate, gravé que d'un coté ne se plie pas bien dans l'autre sens mais finalement il se plie assez bien, par contre ca reste une forme très rigide. 

![test](images/diamondtest2.jpg)

Une fois sur la lampe, je remarque que le pattern Diamond ne se compresse malheureusement pas dans le sens souhaité, le polycarbonate est différent du papier. Il est donc assez satisfaisant visuellement mais ne marche pas pour ce que je veux faire car il n'est pas modulable et n'a pas grand interet pour ma lampe à part habiller l'entièreté de ma lampe. Il faudrait jouer d'avantage sur un pattern de type Miura qui se plie vraiment sur lui-même. 

## PHASE FINALE : EVOLUTION DU PAPIER

### UN PEU DE NATURE DANS CE MONDE DE PLASTIQUE !!! 

Je viens de tomber sur des documents scientifiques super intéressants !!! Ca prend totalement sens de travailler sur un pliage Miura !! En fait, On retrouve la forme de ce pliage dans la nature à travers la méthode d'évolution des végétaux ! Les feuilles se développe selon ce principe de base et c'est super chouette ! Je retrouve un lien à une forme naturelle que ma réfèrence avait deja en se basant sur la forme donnée par le nombre d'or de la coquille de l'ammonite. Mais là j'utilise une autre forme géométrique basée sur le déploiement ! 

![natuuuure](images/leaf.jpg)

La structure tridimensionnelle de certains végétaux reprend clairement le pliage Miura, les arcs les plus efficaces pour couvrir la totalité de la lampe (essais oranges) sont ceux qui adapotent une forme de losange qui peut évoquer celle d'une feuille. Et là j'ai mon idée !! Je vais faire une lampe imitant le système végétal qui soit déployable. Elle prendrais d'autant plus vie ainsi et aurait plus de sens ! 

### INFLUENCE NATURELLE 

L'influence est constante, innée. On passe notre vie à être influencé par tout, tout le temps, sans même en avoir conscience. Entre l'homme et la nature, il y a une influence réciproque. L'homme pour évoluer à créer des outils en s'inspirant des avantages de certains éléments naturels, et il a impacter le développement de la nature suite à cela. Les objets que l'on trouvent aujourd'hui dans notre quotidien sont tous issuent d'influences liées à la nature et l'exemple de la Gherpe lampe est d'autant plus explicite : elle reprend une forme d'ammonite (molusque céphalopode) et se base sur le nombre d'or, un nombre décrété par l'homme mais existant à l'état naturel. Réaliser un pliage de Miura sera l'occasion de s'inspirer à mon tour des éléments naturels (pliage que l'on retrouve sur les feuilles de charmes) et conserver cette idée de déploiement que j'aimais tant dans mon objet de référence. 

![pliages](images/pliages.jpg)

### SURFACE EN PAPIER

### DES SCIENCES NATURELLES AUX MATHEMATIQUES

Petit retour, hélas, obligatoire aux années lycées pour pouvoir mettre tout cela en forme. J'ai décidé de travailler sur le pliage de Miura, mais cela implique quelques petites notions scientifiques mais aussi théorique pour obtenir la forme souhaitée. Il existe pas mal de documentation à ce propos (bien évidemment en anglais et avec une montagne de calculs scientifiques effrayants, mais quand on s'y plonge ce n'est pas si terrible que ça n'y parrait). Parlons déjà théorie (très très vulgarisée) : 

- Le théorème de Kawazaki est nécessaire pour obtenir des vertices (sommets des pliages) correctes mais il n’est pas une condition suffisante. C’est le théorème de Miura qui aide à trouver les bons angles pour que le pliage puisse devenir totalement plat et aligné en conservant un corps rigide (pour que les sommets puissent s'accoler en forme repliée). Si le pliage ne se referme pas totalement à plat, on parle de pliage en «patte d’oiseau», visuellement similaire à celui de Miura mais dont les propriétés sont différentes.

- Ces caractéristiques ont été utilisées entre autre dans le domaine spatial pour réaliser des panneaux qui se déploient facilement. Avec le pliage de Miura, la figure ne dispose que d’un seul degré de liberté, en clair, la forme se déploie simplement en tirant sur un axe. Il n’y a donc que très peu d’énergie à fournir pour déployer une grande surface. Elle se replie de la même manière. La figure répétée pour réalisée un pliage de Miura est faite de trapèzes isocèles identiques et miroirs. Attention cependant, ce n’est pas parce qu’on réalise un pliage de Miura qu’on obtient des plis qui se chevauchent parfaitement. Il y a des conditions a respecter. Le trapèze doit avoir un certain rapport de longueur. 

- Pour ma part j’ai décidé d’avoir un w supérieur à h, et donc faire en sorte que le pliage se croise en 4 points pour obtenir un décalage et donc favoriser l'aspect convexe du pliage. Cela n’est pas conseillé dans le cas où l’on cherche à avoir un pliage avec une bonne résistance à l’horizontal, or mon pliage sera à la verticale et je cherche à le faire fléchir. Dans un pliage de Miura standard les ratios idéaux consistes à avoir : 
• 50°< béta < 60°
• 90°< alpha < 130°
• 1<a/b<2

- Dans le cas de mon pliage, je veux favoriser l’aspect incurvé du pliage, donc modifier ses propriété en jouant sur un défaut pour en faire un atout. Pour cela, je m’éloigne des ratios idéaux et vais voir du côté du coefficient de Poisson (V). Ce coefficient permet de voir la capacité de contraction d’une matière. En cherchant un peu je découvre que le coefficient de poisson peut être positif ou négatif. Il dépend entre autre de l’angle d’ouverture alpha (notre angle principal qui détermine tout d’après le théorème de Miura). Si le rapport a/b est compris entre 0 et 1, alors le pliage sera  plus fermé et le ratio sera positif. C’est donc qu’il possédera une plus grande capacité à se rétracter et la forme ira, d’après les graphiques étudiés, vers une forme convexe optimale avec un angle de 90°. Bien-entendu, la forme sera moins résistante à une pression horizontale mais ceci n’est pas gênant dans mon cas. Je pars donc sur un alpha à 90° et un rapport a/b légèrement inférieur à 1. De là, je cherche à travailler avec des nombres le plus entiers possible pour simplifier les calculs, je pars sur ceux-ci : 

![cercle](images/découpe12.02.210.jpg)

Je réalise donc le cercle à droite sur **Illustrator** en trouvant les mesures un peu à taton. Je fais un premier pliage "maison" et celui ci fonctionne parfaitement ! C'est réussit, ça fonctionne du premier coup ! J'entamme alors les découpes laser pour graver les lignes de pliage sur mon papier (j'ai choisi un papier léger SHIN INBE, apprécié pour sa texture hétérogène, sa couleur, de grammage 65G/m², soit le plus fin mais solide possible). 

![papier test](images/découpe12.02.211.jpg)

Le premier test est le plus prudent pour voir comment réagit la machine, mais je peux légèrement augmenter, ou faire plusieurs passes, car le laser ne grave presque pas et les découpes correspondent plus à de la gravures. 

![papier test 2](images/découpe12.02.217.jpg)

Deuxième tentative avec du papier plus épais et un fin comme le premier coloré mais de plus petite taille. J'augmente légèrement la puissance pour le papier plus épais (le même mais en grammage 105G/m²) et diminue un peu la vitesse. La découpe/gravure est très rapide (8minutes environ). 

![résulta test1](images/découpe12.02.212.jpg)

Voilà les résultats du premier test de découpe après avoir réadapté les valeurs pour le laser. Le pliage se fait facilement grâce à la gravure, malgré le fait que cela ne soit fait que sur une seule face. On passe maintenant au tests lumière, puisque c'est aussi ça qui importe ! 

![tests lumières](images/découpe12.02.213.jpg)

Les résultats sont très satisfaisants, la couleur légèrement jaune/écrue du papier rend le pliage plus chaleureux. On voit bien la trace de gravure plus brune qui marque les sommets, mais ce n'est pas gènant, au contraire je trouve ça esthétique. La manière de plier le papier fait varier la diffraction de la lumière dans celui-ci est c'est très satisfaisant. 

Les résultats en vidéo de mon test avec de la lumière sur [youtube](https://youtu.be/eSs3e1tTlQY).

[![](http://img.youtube.com/vi/eSs3e1tTlQY/0.jpg)](http://www.youtube.com/watch?v=eSs3e1tTlQY "déploiement")

 Pour ajouter des vidéo youtube comme moi, tu peux aller [ici](http://embedyoutube.org/) pour te faciliter la tache. 

### LA STRUCTURE 

### INFLUENCES LUMINEUSES 

Je cherche à faire une lampe déployable qui fonctionne sur un axe. Seule la surface en papier faisant office d'abat jour sera mobile. Je dois alors avoir un axe ou se trouvera la lumière et deux objets coulissants pour faire déployer ma lampe (au moins un). Pour cela, je souhaite m'influencer des anciennes lampes industrielles réglables concernant la fixation de l'axe, mais aussi des lampes Parentesi, produite par les designers italiens Achille Castiglioni et Pio Manzù, produites par Flos, qui sont des lampes coulissantes et réglables qui fonctionne par la simple torsion d'un tube et le frottement de celui-ci sur un câble. Je souhaite également m'inspirer des lampes en papier asiatique, pour leur sobriété, mais aussi leurs légèreté. Sur l'exemple ci-dessous, on peut voir l'exemple des lampes de Isamu Noguchi, un designer americano-japonais.

![influ](images/découpe12.02.219.jpg)

### PIECES DE LA STRUCTURE

Pour construire cette structure, j'ai imaginée une une forme basée sur les dimensions de la gaine que j'ai montée. J'expliquerai la modélisation par la suite. L'idée c'est de faire une suspention murale pour jouer sur les effet de lumière et la gestion de celle-ci. Pour cela, j'ai conçu un espace accueillant l'ampoule et tout le système éléctrique permettant de devenir le support des axes de déploiement tubulaires, et de ce fait, la surface en papier pliée : 

![pièces](images/découpe12.02.214.jpg)

Une vidéo super interessante des pliages surprenant d'un point de vue de la physique :

[![](http://img.youtube.com/vi/VQXKgG7tsII/0.jpg)](http://www.youtube.com/watch?v=VQXKgG7tsII "Itai Cohen explains the physics of origami")

### ASSEMBLAGE

![étapes1](images/découpe12.02.215.jpg)

D'abord on dénude les fils de la gaine sur environ 5 cm pour faire passer les cables dans la suspention murale. On les fait passer à l'intérieur du premier percement dans le tube. Puis on les glisse à l'intérieur de la vis de fermeture de la douille de la gaine. On assemblera les élément dans l'ordre suivant : tube > vis creuse > bas du culot > fiches de la douille > haut du culot > ampoule. 

![étapes2](images/découpe12.02.216.jpg)

On prend soin de laissé un peu de mou sur les cables pour remettre les petites vis servant à maintenir les câbles dans les fiches de la douille. Ensuite un revis le tout et on adapte la taille du dénudement des fils arrières pour des raisons esthétiques. La fixation murale de la lampe jaune garanti le bon maintient de la partie lumineuse car elle forme un écrou au centre du système lumineux, mais sert aussi de fixation au tube. Le percement où passe les fils dans le tubes assure la bonne cohésion des deux éléments. J'ajoute l'ampoule (classique et assez puissante pour traversé le papier) et voilà on à la base du système ! 

![suspention murale](images/découpe12.02.218.jpg)

J'ajoute les barres en métal creuses coupées à 30 cm, de 6 mm de diamètre, achetée à Schleiper. Je les ressère grâce au vis imprimées en 3D après avoir glissé le pliage en papier dans les plaques coulissantes évidemment. Je fixe les deux oreillettes du pliage dans les encoches et fait traverser le tube pour solidariser le tout. J'installe le module final au mur grace à deux clous et voilà l'installation mise en place. 

![test lumière](images/découpe12.02.2110.jpg)

Avec une mise en lumière, on se rend bien compte du résultat final, lorsqu'on regarde la lampe de profil, on peut bien voir le dregré d'ouverture/fermeture du papier et donc l'aspect convexe qu'il adopte. Lorsque la lampe est totalement déployée elle forme un cercle qui diffuse une lumière tamisée et douce sur le mur qui est relativement agréable. Sur les photos, il fait encore jour et pourtant la lampe apporte déjà une ambiance agréable. 

![test lumière 2](images/découpe12.02.2111.jpg)

Maintenant, vue de face, les résultats sont très satisfaisants aussi ! Même de jour, on peut apprécier les effets lumineux provoqués par le replie du papier, et donc les nuances d'éclairement dues à l'épaisseur de papier superposé. En plus de cela, la forme varie constamment. On passe d'une forme circulaire semblable à un soleil, à celle d'une sorte de papillon qui replierait ses ailes.  

### DE L'APPLIQUE MURALE AU CABLE 

Je suis très contente du résultat mais j'aimerai alléger encore plus le rendu visuel et augmenter les possibilités de hauteur de lampe. Parce que lorsque la lampe d'ambiance est murale, cela signifie qu'elle n'a qu'une position d'éclairage fixe. Pour améliorer le rendu final, je me penche sur la référence de la lampe Parentesi réalisée par Achille Castiglioni & Pio Manzù, produite par Flos. Cette lampe est particulièrement intéressante car elle propose une ampoule mobile le long d'un câble tendu du plafond au sol. Pour pouvoir stabiliser l'ampoule à un niveau précis du câble, les designers ont eut une idée révolutionnaire : 

![parentesis lampe](images/parentesi-suspension-castiglioni-manzu-flos-F54000-product-life-01-1440x802-1.jpg)

Les câbles tendus du sol au plafond provoques des frottements en passant dans le tube qui maintient la lampe. Les frottements et la torsion du câble en acier garantissent au tube de ne pas glisser, cela est accentué par l'effet "coudé" du tube qui augmente les frottements. Ainsi, la lampe peut aisément se mouvoir le long du fil, être stoppée à la hauteur souhaitée, et conserver sa position. 

## REALISATION FINALE 

- **L'ABAT-JOUR** : Le patron conserve les mêmes proportions que celles établies plus haut. Je l'ai réalisé sur **Illustrator**, ou **Inskape**. Je vous propose donc les dimensions minimales de la lampe afin que celle-ci puisse se plier et se déployer. Si elle était plus petite, elle risquerait de ne pas s'adapter assez pour que le système puisse totalement se replier sur lui-même. Le risque, si elle est trop grande, c'est qu'elle ne plie plus correctement. Dans le cas où vous souhaiteriez l'agradir, il est clairement conseillé de conserver les angles calculés et préférable de conserver l'homothétie du dessin afin d'agrandir les tesselations et pas simplement quelques traits. Par ailleurs, si la tessellation devient trop grande, la poussée ne sera plus assez forte pour replier correctement le dessin sur lui-même, donc il ne faut pas abuser de l'agrandissement. Je n'ai pas calculé les dimensions maximales possible pour le patron, car les calculs seraient très très complexes. Un cercle de 50 cm est une bonne taille de toute manière je trouve pour cette lampe. Ci-contre, le patron du cercle : [patron](images/patron_pour_gravure_et_découpe__cercle_.svg) Pourquoi un cercle ? Parce que c'est la forme qui, repliée, varie le plus d'apparence. J'ai aussi essayé le carré mais la forme est beaucoup moins efficace car la quantité de papier en plus sur les pourtours restreint le pliage en ajoutant du poid. Cela aurait été la même chose pour d'autres formes. Le cercle reste selon moi, la forme idéale. 

- **LA STRUCTURE COULISSANTE** : La structure coulissante qui s'articulera autour du câble sera influencée par la lampe Parentesis. Elle reprendra le principe de coudé tubulaire, système mécanique intéressant pour mon cas. Pour cela, j'ai dessiné le modèle 3D sur **Fusion 360**. Pour le système d'accroche de l'abat-jour en papier, j'ai pensé à faire une attache simple qui permettent le démontage de l'abat-jour pour le changer. J'ai alors imaginé une sorte de presse-papier coulissant dans lequel viendra s'intercaler le papier. L'union des parties inférieure et supérieure de ce presse papier formera un cylindre qui viendra se bloquer dans l'accroche mobile tubulaire. 

### MODELISATION 3D 

![1](images/fig1.JPG)

1 - Pour dessiner le système d'accroche du papier : Commencer par dessiner le cylindre qui accueillera le presse-papier. Prevoyez un cercle de 10mm d'épaisseur, et 15mm de diamètre. Créer une ouverture de 20mm dans le tube qui deviendra l'encoche pour glisser le système d'accroche. Pour le presse papier, faire un cercle inscrit dans le tube de 9.7mm (les 3mm de différence correspondent au jeu, garantissement un bon coulissement avec l'épaisseur du papier). Prolonger les parties presse-papier de 30 mm afin de bloquer les languettes. Vous obtenez le profil du système d'accroche. 

![2](images/fig2.JPG)

2 - Extruder le cylindre d'accroche symétriquement de 7.5 mm afin que le cyclindre fasse 15mm de hauteur. 

![3](images/fig3.JPG)

3 - Extruder de 35 mm, de manière symétrique, les plaques du presse papier. de manière à ce que ceux-ci face 70mm de longeur. 

![4](images/fig4.JPG)

4 - Faire trois cercles de 4mm de diamètre sur la plaque, et extruder ses cercles sur une des plaques du presse papier pour créer des ouvertures, pas sur l'autre. 

![5](images/fig5.JPG)

5 - Sur la seconde plaque, dessiner les mêmes cercles de 4mm de diamètre en symétrie, puis extruder ces cercles sur 6mm d'épaisseur afin de créer des "picots" qui s'emboite dans le papier, puis dans la seconde plaque. 

![6](images/fig6.JPG)

6 - Faire une esquisse sur le plan XY de 120mm de long (30mm droit, puis 14.15mm à 45°, puis 40mm, puis 14.15mm à -45°, puis 30mm droit). Dessiner ensuite sur le plan ZX deux cercles inscrits pour former un tube de 15mm de diamètre, percé de 10mm de diamètre au centre. 

![7](images/fig7.JPG)

7 - Créer un balayage en suivant le chemin de la remière esquisse et le profil du tube afin de créer le volume de notre tube coulissant. Puis ajuster le cylindre d'accroche dessiné au début de manière à ce qu'il tombe précisément dans l'épaisseur du tube afin que les deux objets soient liés mais ne devienne pas un obstacle dans le creux du tube.

![8](images/fig8.JPG)

8 - Dupliquer l'ensemble. Vous avez désormais obtenu les deux tubes coulissants servant d'accorche à l'abat-jour en papier. Il ne reste plus qu'à créer le tube pour la lampe. 

![9](images/fig9.JPG)

9 - Créer un autre balayage pour fabriquer un second tube en selectionnant la ligne comme chemin et le tube comme profil à prolonger. Vous obtenez le tube pour accrocher la lampe. 

![10](images/fig10.JPG)

10 - Dessiner un cercle tengeant du diamètre de votre ampoule, pour ma part, il s'agit d'un cercle de diamètre 35mm intérieur et 50mm de diamètr extérieur. Les cercles doivent etre tangeant au tube et l'épaisseur doit fusionner avec le tube de sorte à lier les deux objets. 

![11](images/fig11.JPG) 

11 - Extruder symétriquement le cercle pour en faire un cylindre creux de 75mm de chaque coté. Soit 150mm au total. 

![12](images/fig12.JPG)

12 - Vous obtenez toutes les pièces nécessaires à la réalisation de la lampe, il ne reste plus qu'à imprimer. Pour faciliter l'impression, lancer les presse-papiers avec des supports séparemment et les tubes coulissant sans, ils n'en ont pas besoin. Pour cela, masquer les corps et exporter en STL sur **PrusaSlicer**. Vous pouvez gagner encore plus de temps en mettant un remplissage à 5% et en écartant les supports à 8mm. Sinon les paramètres restent standard. J'ai monté les impression à une vitesse de 130% pour accèlérer le processus, et les impression sont propres. Toujours supprimer la boucle pour éviter une mauvaise impression 3D. 


### MULTIPLICITE DES AMBIANCES PAPIERS 


Pour le papier, j'avais essayé avec du papier 65Gr/m² mais celui-ci s'avérait limite : très fin, il s'habimais rapidement avec les multiples transports et manipulations. Même si ce n'aurait pas été une situation réaliste pour une vraie lampe, cela montre bien l'effet que le papier aurait eu avec le temps et l'usure du mouvement. La fragilité venait principalement apparaitre au niveau des languettes d'accroches, plus soumises aux contraintes. Pour résoudre une partie du problème, j'ai décidé de faire des languettes plus grandes repliées en deux pour solidifier la zonne. Cela s'est avéré être une bonne solution, mais je tenais quand même à essayer avec du papier plus épais. Le vendeur m'a donc conseillé un grammage au dessus (105Gr/m²), un papier utilisé pour des abat-jours en origami fixe. 

![13](images/fig13.JPG)

Le papier état plus épais j'augmente la puissange du laser à 2.5 pour les gravures intérieures. Mais comme on le voit sur la première, le papier se déchire sur les zones fragilisées par la gravure lors du pliage car il est trop rigide. Un papier un peu moins cartonné est préférable. Les autres tests qui suivent sur les photos sont aussi en 105Gr/m², mais avec des passes plus basse et répété pour plus de sécurité. La gravure est moins profonde, donc le pliage plus compliqué mais on y arrive tout de même. Par contre, l'opacité du papier fait qe le luminaire devient fonctionnel uniquement de nuit. Un grammage compris entre 65 et 105 Gr/m² est donc idéal avec de préférence une gravure à plusieurs passe basse par sécurité. 

### RECYCLAGE

Après tout, pourquoi acheter du papier quand on en a tout autour de nous et qu'il fini à la poubelle ? J'essaie de trouver quelque chose qui traine dans mon appartement qui correspond au mesures. Je tombe sur une notice de médicament qui a presque les bonnes mesures. J'adapte alors mon patron pour le réduire légèrement de 5cm. Par contre je ne diminue pas la taille des languettes sinon les trous ne tomberont pas au bon endroit pour les accroches. 

![14](images/fig14.jpg)

La découpe laser est un peu plus laborieuse comme le papier possède des plis marqué, je le tend sur une surface rigide et le scotch pour m'assurer qu'il soit bien à plat. J'y vais très doucement sur la gravure car le papier est fin. Mais finalement ça fonctionne. Je dois quand même revenir sur ma gravure manuellement à quelques endroits pour rectifier les endroits qui n'ont pas très bien marché. Le ésultat est génial. Ca donne un super style à la lampe, et la finesse du papier rend la lumière plus diffuse. Dommage qu'il soit un peu trop petit et ne puisse pas du coup se replier totalement. L'aspect un peu plastifié/glacé du papier le rend plus souple et solide qu'un papier au grammage fin acheté en papeterie. 

![15](images/fig15.jpg)

![16](images/fig16.jpg)

PS : il faut noter que le morceau de bois que l'on voit derrière n'existe pas en vrai, il sert uniquement à la fixation mobile de la lampe pour pouvoir l'exposer lors de mon jury. Il faut le prendre en compte car il allourdit clairement le visuel final. La lampe, dans de vraies conditions, est bien plus aérienne que cela. Elle gravite le long d'un cable qui s'efface dans l'espace. Ici il a été difficile de faire de bonnes photos car je ne dispose pas de la possibilité de faire des trous dans mon plafond pour la fixer et pour l'exposition, je préfère avoir un support pour faire une démonstration de la lampe. Ci dessous, une idée de ce à quoi resssemblerait la lampe dans de vraies conditions : 


![final](images/FINAL_image_design.jpg)


Maintenant, à toi de jouer ! Fabriques ta propre lampe en la personnalisant. Il te suffit de reprendre mes fichiers STL et SVG tout en haut de la page, et choisir le papier que tu souhaites pour l'ambiance que tu désires. Tu peux jouer sur la taille du pliage pour encore plus d'effet. 




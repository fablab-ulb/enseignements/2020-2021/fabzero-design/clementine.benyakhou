# 2. MODULE 2 : DESSIN CAO

Avant de parler **imprimante 3D** (ce sera pour le module 3), je vais déjà revenir sur un logiciel dont j'avais parlé dans le module 1 : **Fusion 360**. C'est LE logiciel qu'on utilise pour modéliser un objet en 3D pour l'imprimer ensuite. Il n'est pas le seul mais c'est celui que l'on va utiliser nous durant l'option Design. Ainsi, j'ai du modéliser mon objet (pour rappel il s'agit de la lampe Gherpe de Superstudio) à **échelle réelle**. Pour ce faire, j'ai du comprendre l'objet et sa matérialité. 

## MATERIALITE

La structure principale qui encercle la lampe est réalisée en **métacrylate** (acrylique) teinté translucide. Cela lui confère une légèreté materielle et visuelle. Le fait que ce soit modulable permet à la fois de jouer sur l'intensité d'éclairement, mais aussi sur l'opacité/translucidité de la lampe en additionnant ou soustraillant des couches par rotation.

Sa forme m'évoque celle d'un **nautil**, animal aquatique marin, dont les proportions correspondent à la suite de **Fibonacci** (voir photo ci-dessous. En mathématiques, la suite de Fibonacci est une suite de nombres entiers dans laquelle chaque terme est la somme des deux termes qui le précèdent. Cette suite, graphiquement, reprend les proportions idéales du nombre d'or et donne le carré d'or. Un point interessant qui pourra être développé au cours de mon travail. 

![Fibonacci](../images/nbror.jpg)

## L'IMPORTANCE DE LA LECTURE

Une **bonne lecture** de l'objet est primordiale. Lorsque j'ai commencé à rentrer un peu plus dans la compréhension de cette lampe, j'ai d'abord essayé de la modéliser un peu aléatoirement sur Fusion 360. Ce fut compliqué. Il faut savoir que les informations concernant les dimensions de la lampe sont très faibles. En effet, on ne connait que 3 de ses dimensions, qui correspondent aux dimensions maximales extérieures (une fois déployée) : 53 x 31 x 38(h) cm. Impossible donc de savoir exactement la dimension des "arcs" modulables. Je me suis vite rendu compte, après les conseils de notre enseignant qu'il était essentiel de comprendre comment cette dernière était faite, pour **appréhender** l'objet en lui même. En effet, ma lampe est réalisée dans un matériau fin (le métacrylate). Cette matière à une grande importance dans le processus de réalisation de l'objet en soit. Il ne s'agit pas d'un matériel très souple donc ce dernier ne subit pas une simple torsion maintenue sous l'effet de la traction de la structure primaire. Au contraire, comme on peu le voir ci-dessous : 

![système](../images/système.jpg)

Les "arcs" de la lampe possèdent une certaine rigidité, et une vraie épaisseur. Ils ne sont pas en traction dans le système métalique central mais plutôt maintenus en **compression** par celui-ci. Cette rigidité et la recherche autour du matériau m'amène à conclure que les arcs sont **pliés à chaud**, c'est-à-dire que le matériau est chauffé pour être courbé et adopter sa forme définitive. 

Malgré le manque d'information concernant les dimensions de la lampe, en savoir plus sur sa matérialité m'a aidé à déduire un premier élément : l'**épaisseur** de ses arcs. En me rendant sur le site du principal fabriquant de métacrylate, j'ai pu, grâce au descriptif, supposer que l'épaisseur de la plaque servant à faire les arcs était de 3mm d'épaisseur : assez fin pour être translucide, facilement chauffable et déformable, et conserver de la légèreté visuelle. Après vérification, il s'agit bien de **Perspex** (métacrylate) découpé au laser et plié à chaud. En partant de là, j'ai pu déduire le reste. D'où l'importance de vraiment comprendre le système de conception de l'objet pour mieux l'appréhender. 

Aussi j'ai décidé de fixer les mesures suivantes d'après mes dernières déductions et une estimation graphique : 

![dimensionnement](../images/dimensions.jpg)

J'ai ainsi pu commencer à modéliser la 3D de ma lampe sur le logiciel **fusion 360** de la manière la plus précise et réelle possible. Pour ce faire, j'ai travaillé avec les dimensions établies et choisi de la configurer repliée, c'est-à-dire, avec tous les "arcs" posés sur un même plan, semblable à l'image montrant le système intérieur plus haut, par soucis de facilité de représentation mais aussi de production. 

## PRECISIONS, DETAILS

Les arcs étant fixés, il m'a fallu ensuite entrer dans le **mécanisme** pour appréhender le système intérieur. Finalement le propre de cette lampe c'est quoi ? C'est de la modularité et de la liberté au niveau du positionnement des arcs, aux déclinaisons presque infinies ! Cette lampe, semble si évidente, mais ne l'ai pas tant que ça. Elle adopte une forme simple, une prise en main intuitive et pourtant une **reflexion** bien travaillée derrière. Quoi de plus frustrant qu'une lampe qu'on admire pour sa modularité mais qu'on ne peut pas toucher ? Je crois que son inaccessibilité renforce mon envie de la comprendre mieux.

Finalement qu'est ce que je sais et qu'est ce que j'en déduis ? J'observe une modularité par la **rotation**, ça peut sembler évidant mais ça a son importance : il y a donc une tige de rotation ! Et ça je n'y est pas pensé tout de suite. Cet axe est lisse puisque chaque pièce tourne indépendemment des autres. On sait que les arcs ne sont pas en tractions sur cet axe puisque leur forme est "figée" par la matière. donc ils tournent autour de cet axe. On sait aussi qu'il y a deux "**boules**" qui ressemblent à des poignées. Ces boules, succèdées par des rondelles "ferment" les axes de rotation. Comment je sais qu'elles le ferme ? Parce que de l'autre de côté de ce système, l'axe débute avec une tête plate qui laisse penser qu'il s'agit d'une sorte de de **boulon** lisse. Les boules seraient alors les écrous de ses axes (boulons). Tout ça semble logique et cohérent. Ainsi j'en déduit cela : 

![détails](../images/détails.jpg)

Ce petit exercice m'a clairement aidé à concevoir la modélisation 3D de mon objet mais surtout à le saisir, me sentir plus **proche** de lui. Cela me fais me rendre compte que je ne cherche pas assez à comprendre comment sont conçues et imaginées les choses qui m'entourent dans la précision. Aussi, vous pouvez désormais suivre mes étapes pour la modéliser.



## MODELISATION DE L'OBJET

Il a fallu passer par différentes étapes, j'ai d'abbord réfléchi au moyen le plus efficace pour représenter cette lampe : il s'agissait de la concevoir repliée pour que les différents ensembles se retrouvent un maximum appuyés sur un même plan, ce qui est idéal pour une impression 3D. Il fallait aussi que ma lampe puisse être faite à l'impression 3D en **une seule fois**. J'ai alors imaginer le mécanisme de rotation de cette dernière de manière à ce qu'elle soit modulable. Ce fut un véritable défi pour moi. J'ai ensuite testé les différentes fonctionnalités de **Fusion 360** pour modéliser ma lampe. Je vais vous détailler les étapes de celles qui m'ont semblé les plus être les plus pertinantes et efficaces pour parvenir à mon objectif :

### Dessiner les arcs en plan

![première étape](../images/ex1.jpg)
- 1 : Pour dessiner les arcs, c'était assez simple, avec les mesures déduites de la lampe, je traçais le rectangle dans lequel allait s'inscrire chaque arc, puis j'y inscrivais le cercle correspondant à la pliure du matériaux. 
- 2 : En utilisant l'outil ciseaux "ajuster", j'ai ensuite supprimé les traits de construction inutile pour ne garder que la partie de l'arc de manière à ce que celui-ci soit de 3mm d'épaisseur et adopte la taille que je lui ai déduite. 
- 3 : J'ai renouvelé l'opération, toujours en plan jusqu'à avoir mes 6 arcs de taille différentes, avec un espace de 5mm entre chaque pour que l'impression laser soit possible à une petite échelle. 

### Passage à la 3D

![deuxième étape](../images/ex2.jpg)
- 4 : J'ai terminé l'esquisse 2D pour pouvoir passé en 3D. La hauteur des arcs correspond à 20cm d'après mes calculs. J'ai séléctionné toutes les surfaces remplies des arcs et utilisée l'outil "extrusion d'un solide" pour allonger mes surfaces en 3D. J'ai ainsi obtenu mes 6 arcs en 3D. 
- 5 : Ensuite il a fallu arrondir le bout de mes arcs, plutot que de travailler sur tous les arcs un par un comme je pensais le faire au début, j'ai décidé de simplifier la tache en tracant sur le premier plan de mon arc l'arrondi du bout de celui-ci, de la même manière que précédemment : c'est-à-dire en inscrivant un cercle et supprimant avec les ciseaux "ajuster" l'autre partie de mon cercle. 
- 6 : Par après, j'ai réutilisé l'outil "extrusion d'un solide" mais cette fois ci en jouant sur le négatif pour soustraire la matière des volumes. Ainsi, j'ai séléctionné mes surfaces à supprimer et les ai tiré jusqu'à la suppression totale de ses dernières.

### Système d'articulation de la lampe

![troisième étape](../images/ex3.jpg)
- 7 : Avant toute chose, une fois les arcs fait, il fallait pouvoir faire passer le système d'articuliation dans mes arcs. D'après l'analyse de mon image, ce système plutôt simple semblait correspondre à une fixation d'à peu près 4mm. J'ai ainsi réalisé un percement pour l'axe de rotation dans mes arcs au centre du cercle inscrit de 6mm pour permettre un marge de rotation et surtout la séparation de mon système rotatif avec le reste des pièces. 
- 8 : j'ai ensuite conçu les pièces utiles à la rotation : le système écroux-boulon (expliqué dans le détail de mon projet final). J'ai aussi imaginé une rondelle pour resserrer le système de rotation, dont le centre est percé lui aussi de 6mm pour pouvoir rendre la pièce indépendant du système rotatif. Seule la partie boulon-écroux est faite d'une seule partie soudée car il aurait été trop compliqué de faire le pas de vis sur la modélisation 3D à une petite échelle. 
- 9 : Par la suite, j'ai dessiné le dernier élément métalique sur lequel est fixé l'ampoule, lui aussi modulable. qui permet de resserrer le système rotatif de l'autre coté et faire que les arcs soit en compression entre lui-même et le système de sérrage (boulon en forme de boule). Pour cela j'ai encore une fois dessiné l'arc en plan puis joué avec l'outil "extrusion" pour lui donner ça hauteur de 3cm. 

### Ajustement des accessoirs

![quatrième étape](../images/ex4.jpg)
- 10 : Avec l'outil déplacement, j'ai ajusté la pièce que j'avais travaillé au sol pour qu'elle se trouve elle aussi dans l'axe rotatif du système. Pour cela j'ai "combiné" les différents éléments du groupe pour que la pièce soit un ensemble, j'ai séléctionné l'ensemble puis fait un clic droit et appuyé sur "déplacé", j'ai travaillée en coupe pour m'assuré que l'élément soit parfaitement centré sur l'axe de rotation. 
- 11 : j'ai également vérifié en plan voir si cela était là aussi centré et ajusté puisque ça ne l'était pas. J'ai également séléctionné tout le système rotatif (je conseil d'ailleurs de donner des noms à chaque pièce de l'objet nommés "corps" sur le logiciel pour s'y retrouver), que j'ai dupliqué avec une rotation à 180° (faisable par un clic droit aussi). 

### Montage final 

![dernière étape](../images/ex5.jpg)
- 12 : j'ai fais la même chose pour le système rotatif : un ajustement au niveau de la hauteur sur les plans x et y de manière à ce qu'ils se retrouvent au centre de l'axe de rotation. Pour ça j'ai d'abord renommé les corps, les ai "combiné" pour ne faire qu'un et déplacé. 
- 13 : voilà, l'objet final étant mis au point, il semble bon au niveau des suppositions de dimensionnement. Je suis satisfaite du résultat, il ne reste plus qu'à voir ce que ça donne au niveau de l'impression 3D. 


## NOTA BENE

Les détails que je vous donne là ont été fait lors de ma deuxième tentative pour m'assurer, après mes premiers échecs d'impression 3D (voir module3), que je n'avais pas fait d'erreur dans la modélisation. Ma première tentative n'a pas été aussi fluide pour modéliser simplement mon objet, j'ai eu le même processus de cheminement, mais tout en testant et découvrant les outils de l'application. Il y a donc eu plus de retour en arrière et d'essaie pour arriver à cela. Ma modélisation était là même mais en plus laborieuse le temps de vraiment connaître le logiciel. La refaire m'a permis de vraiment être certaine que le beug d'impression ne venait pas de là, mais aussi me familliariser d'autant plus avec le logiciel. 

Maitenant, je vous invite à lire le module 3 pour découvrir les dits "échecs" mais aussi ma découverte et mon amélioration à l'impression 3D...
